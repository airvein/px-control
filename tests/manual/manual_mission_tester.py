import threading

import rclpy
from std_msgs import msg as std_msg

from tests.unit.fixtures.normal_route import normal_route_fixture
from tests.unit.fixtures.emergency_route import emergency_route_fixture
from tests.unit.fixtures.rth_route import rth_route_fixture
from px_control.common.utils import get_timestamp


class ManualMissionTester:
    def __init__(self):
        rclpy.init(args=None)
        self.node = rclpy.create_node('ManualMissionTester')

        self._create_publishers()
        self.heartbeat_timer = self.node.create_timer(
            1, self._publish_heartbeat)

        self.spin_thread = threading.Thread(target=self._spin, args=(), daemon=True)
        self.spin_thread.start()

    def normal_mission(self):
        msg = std_msg.String(data=normal_route_fixture)
        self.route_normal.publish(msg)

    def emergency_mission(self):
        msg = std_msg.String(data=emergency_route_fixture)
        self.route_emergency.publish(msg)

    def rth_mission(self):
        msg = std_msg.String(data=rth_route_fixture)
        self.route_rth.publish(msg)

    def start_mission(self):
        msg = std_msg.Empty()
        self.start_mission_publisher.publish(msg)

    def stop_heartbeat(self):
        self.heartbeat_timer.cancel()

    def resume_heartbeat(self):
        self.heartbeat_timer.reset()

    def _publish_heartbeat(self):
        msg = std_msg.UInt64(data=get_timestamp())
        self.heartbeat_publisher.publish(msg)

    def _create_publishers(self):
        self.route_normal = self.node.create_publisher(
            std_msg.String, "/flight_control/route_normal", qos_profile=10)
        self.route_emergency = self.node.create_publisher(
            std_msg.String, "/flight_control/route_emergency", qos_profile=10)
        self.route_rth = self.node.create_publisher(
            std_msg.String, "/flight_control/route_rth", qos_profile=10)
        self.start_mission_publisher = self.node.create_publisher(
            std_msg.Empty, "/flight_control/start_mission", qos_profile=10)
        self.heartbeat_publisher = self.node.create_publisher(
            std_msg.UInt64, "/flight_control/heartbeat", qos_profile=10)

    def _spin(self):
        try:
            while rclpy.ok():
                try:
                    rclpy.spin_once(self.node)
                except KeyboardInterrupt:
                    break
        finally:
            rclpy.shutdown()

"""test_flight_control_route_normal.py

Unit tests for FlightControlRouteNormal subscriber class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node
from std_msgs import msg as std_msg

from px_control.command_interpreter.subscribers import FlightControlRouteNormal
from px_control.common.constants import CommandNames, MissionTypes
from px_control.mavlink_controller.commands_manager import CommandsManager


class TestFlightControlRouteNormal(unittest.TestCase):
    def setUp(self) -> None:
        self.node = MagicMock(spec=Node)
        self.commands_manager = MagicMock(spec=CommandsManager)

        self.subscriber = FlightControlRouteNormal(
            node=self.node, commands_manager=self.commands_manager
        )

    def test_class_should_create_subscription_when_initialized(self):
        self.node.create_subscription.assert_called_with(
            std_msg.String,
            "/flight_control/route_normal",
            self.subscriber.handle)

    def test_handle_should_upload_mission(self):
        mock_message = std_msg.String(data='route_data')

        self.subscriber.handle(mock_message)

        self.commands_manager.add.assert_called_once_with(
            CommandNames.UPLOAD_MISSION, MissionTypes.NORMAL, mock_message.data
        )

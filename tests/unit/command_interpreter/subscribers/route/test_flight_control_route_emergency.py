"""test_flight_control_route_emergency.py

Unit tests for FlightControlRouteEmergency subscriber class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock, call

from rclpy.node import Node
from std_msgs import msg as std_msg

from px_control.command_interpreter.subscribers import \
    FlightControlRouteEmergency
from px_control.common.constants import CommandNames, AutopilotModes, \
    MissionTypes
from px_control.mavlink_controller.commands_manager import CommandsManager


class TestFlightControlRouteEmergency(unittest.TestCase):
    def setUp(self) -> None:
        self.node = MagicMock(spec=Node)
        self.commands_manager = MagicMock(spec=CommandsManager)

        self.subscriber = FlightControlRouteEmergency(
            node=self.node, commands_manager=self.commands_manager
        )

    def test_class_should_create_subscription_when_initialized(self):
        self.node.create_subscription.assert_called_with(
            std_msg.String,
            "/flight_control/route_emergency",
            self.subscriber.handle)

    def test_handle_should_change_mode_to_brake_upload_mission_and_change_to_auto(self):
        mock_message = std_msg.String(data='route_data')

        self.subscriber.handle(mock_message)
        expected_calls = [
            call(CommandNames.SET_MODE, AutopilotModes.BRAKE),
            call(CommandNames.UPLOAD_MISSION,
                 MissionTypes.EMERGENCY, mock_message.data),
            call(CommandNames.SET_MODE, AutopilotModes.AUTO)
        ]

        self.commands_manager.add.assert_has_calls(expected_calls)

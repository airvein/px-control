"""test_flight_control_emergency_kill.py

Unit tests for FlightControlEmergencyKill command subscriber class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node
from std_msgs import msg as std_msg

from px_control.common.constants import CommandNames
from px_control.mavlink_controller.commands_manager import CommandsManager
from px_control.command_interpreter.subscribers import FlightControlEmergencyKill


class TestFlightControlEmergencyKill(unittest.TestCase):
    def setUp(self) -> None:
        self.node = MagicMock(spec=Node)
        self.commands_manager = MagicMock(spec=CommandsManager)

        self.subscriber = FlightControlEmergencyKill(
            self.node, self.commands_manager)

    def test_class_should_create_subscription_when_initialized(self):
        self.node.create_subscription.assert_called_with(
            std_msg.Empty,
            "/flight_control/emergency_kill",
            self.subscriber.handle)

    def test_handle_should_terminate_flight(self):
        self.subscriber.handle(std_msg.Empty())

        self.commands_manager.add.assert_called_once_with(
            CommandNames.TERMINATE_FLIGHT
        )

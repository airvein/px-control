"""test_abstract_command_subscriber.py

Unit tests for AbstractCommandSubscriber class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node

from px_control.mavlink_controller.commands_manager import CommandsManager
from px_control.command_interpreter.subscribers.commands.abstract_command_subscriber import \
    AbstractCommandSubscriber


class TestAbstractCommandSubscriber(unittest.TestCase):

    def setUp(self) -> None:
        self.node = MagicMock(spec=Node)
        self.commands_manager = MagicMock(spec=CommandsManager)

        self.abstract_subscriber = AbstractCommandSubscriber(
            self.node, self.commands_manager)

    def test_abstract_class_should_create_class_objects_from_injected_objects(self):
        self.assertIsInstance(self.abstract_subscriber._node, Node)
        self.assertIsInstance(self.abstract_subscriber._commands_manager, CommandsManager)

    def test_handle_should_raise_not_implemented_error(self):
        with self.assertRaises(NotImplementedError):
            self.abstract_subscriber.handle(None)

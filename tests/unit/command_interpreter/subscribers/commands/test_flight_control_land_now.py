"""test_flight_control_land_now.py

Unit tests for FlightControlLandNow command subscriber class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node
from std_msgs import msg as std_msg

from px_control.common.constants import CommandNames, AutopilotModes
from px_control.mavlink_controller.commands_manager import CommandsManager
from px_control.command_interpreter.subscribers import FlightControlLandNow


class TestFlightControlLandNow(unittest.TestCase):
    def setUp(self) -> None:
        self.node = MagicMock(spec=Node)
        self.commands_manager = MagicMock(spec=CommandsManager)

        self.subscriber = FlightControlLandNow(
            self.node, self.commands_manager)

    def test_class_should_create_subscription_when_initialized(self):
        self.node.create_subscription.assert_called_with(
            std_msg.Empty,
            "/flight_control/land_now",
            self.subscriber.handle)

    def test_handle_should_change_mode_to_land(self):
        self.subscriber.handle(std_msg.Empty())

        self.commands_manager.add.assert_called_once_with(
            CommandNames.SET_MODE, AutopilotModes.LAND
        )

"""test_flight_control_start_mission.py

Unit tests for FlightControlWait command subscriber class.

Copyright 2020 Cervi Robotics
"""
import logging
import unittest
from unittest.mock import MagicMock, call, patch

from rclpy.node import Node
from std_msgs import msg as std_msg

from px_control.common import constants
from px_control.common.constants import CommandNames
from px_control.mavlink_controller.commands_manager import CommandsManager
from px_control.command_interpreter.subscribers import FlightControlStartMission
from px_control.mavlink_controller.mission.emergency_mission import EmergencyMission
from px_control.mavlink_controller.mission.normal_mission import NormalMission
from px_control.telemetry import Telemetry
from drone_types import msg


class TestFlightControlStartMission(unittest.TestCase):
    def setUp(self) -> None:
        self.node = MagicMock(spec=Node)
        self.commands_manager = MagicMock(spec=CommandsManager)
        self.telemetry = MagicMock(spec=Telemetry)
        patch_logger = patch('px_control.command_interpreter.subscribers.commands.flight_control_start_mission.logger',
                             spec=logging.Logger)
        self.logger = patch_logger.start()
        self.addCleanup(patch_logger.stop)

        self.subscriber = FlightControlStartMission(
            self.node, self.commands_manager, self.telemetry)

    def test_class_should_create_subscription_when_initialized(self):
        self.node.create_subscription.assert_called_with(
            std_msg.Empty,
            "/flight_control/start_mission",
            self.subscriber.handle)

    def test_handle_should_not_arm_drone_and_not_start_mission_when_arm_state_is_not_ready(self):
        self.telemetry.status = MagicMock(spec=msg.PxControlStatus)
        self.telemetry.status.arm_state = msg.PxControlStatus.ARM_STATE_NOT_READY
        self.telemetry.current_mission = None

        undesirable_calls = [
            call(CommandNames.SET_MODE, mode=constants.AutopilotModes.GUIDED),
            call(CommandNames.ARM),
            call(CommandNames.START_MISSION)
        ]

        self.subscriber.handle(std_msg.Empty())

        self.commands_manager.add.assert_not_called()

        for undesirable_call in undesirable_calls:
            with self.subTest(msg=undesirable_call):
                self.assertNotIn(
                    undesirable_call, self.commands_manager.add.call_args_list
                )
                self.logger.error.assert_called()

    def test_handle_should_arm_drone_and_start_mission_when_arm_state_ready(self):
        self.telemetry.status = MagicMock(spec=msg.PxControlStatus)
        self.telemetry.current_mission = MagicMock(spec=NormalMission)
        self.telemetry.status.arm_state = msg.PxControlStatus.ARM_STATE_READY

        expected_calls = [
            call(CommandNames.SET_MODE, mode=constants.AutopilotModes.GUIDED),
            call(CommandNames.ARM),
            call(CommandNames.START_MISSION)
        ]

        self.subscriber.handle(std_msg.Empty())

        self.commands_manager.add.assert_has_calls(expected_calls)

    def test_handle_should_not_arm_drone_and_not_start_mission_when_normal_mission_is_not_set(self):
        self.telemetry.status = MagicMock(spec=msg.PxControlStatus)
        self.telemetry.status.arm_state = msg.PxControlStatus.ARM_STATE_READY
        self.telemetry.current_mission = MagicMock(spec=EmergencyMission)

        undesirable_calls = [
            call(CommandNames.SET_MODE, mode=constants.AutopilotModes.GUIDED),
            call(CommandNames.ARM),
            call(CommandNames.START_MISSION)
        ]

        self.subscriber.handle(std_msg.Empty())

        self.commands_manager.add.assert_not_called()

        for undesirable_call in undesirable_calls:
            with self.subTest(msg=undesirable_call):
                self.assertNotIn(
                    undesirable_call, self.commands_manager.add.call_args_list
                )
                self.logger.error.assert_called()

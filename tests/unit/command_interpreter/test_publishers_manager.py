"""test_publishers_manager.py

Unit tests for PublishersManager class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node

from px_control.telemetry import Telemetry
from px_control.command_interpreter.publishers_manager import PublishersManager
from px_control.common.constants import PublisherNames
from px_control.command_interpreter.publishers import *


class TestPublishersManager(unittest.TestCase):
    def setUp(self) -> None:
        self.node = MagicMock(spec=Node)
        self.telemetry = MagicMock(spec=Telemetry)

        self.publishers_manager = PublishersManager(self.node, self.telemetry)

    def test_class_should_have_proper_class_vars_when_initialized(self):
        self.assertIsInstance(self.publishers_manager._node, Node)
        self.assertIsInstance(self.publishers_manager._telemetry, Telemetry)
        self.assertIsInstance(self.publishers_manager.map, dict)

    def test_class_should_create_map_with_publisher_instances_when_initialized(self):
        cases = [
            {"name": PublisherNames.ATTITUDE, "instance_of": Attitude},
            {"name": PublisherNames.BATTERY, "instance_of": Battery},
            {"name": PublisherNames.EKF_STATUS, "instance_of": EKFStatus},
            {"name": PublisherNames.GLOBAL_POS, "instance_of": GlobalPos},
            {"name": PublisherNames.IMU, "instance_of": IMU},
            {"name": PublisherNames.LOCAL_POS, "instance_of": LocalPos},
            {"name": PublisherNames.STATUS, "instance_of": Status},
            {"name": PublisherNames.VELOCITY, "instance_of": Velocity},
        ]

        for case in cases:
            with self.subTest(msg=case):
                self.assertIn(case['name'], self.publishers_manager.map)
                self.assertIsInstance(
                    self.publishers_manager.map[case['name']], case['instance_of']
                )
        self.assertEqual(len(cases), len(self.publishers_manager.map))

    def test_class_should_create_send_alert_publisher(self):
        self.assertIsInstance(
            self.publishers_manager.send_alert, SendAlert
        )

    def test_class_should_create_heartbeat_publisher(self):
        self.assertIsInstance(
            self.publishers_manager.heartbeat, Heartbeat
        )

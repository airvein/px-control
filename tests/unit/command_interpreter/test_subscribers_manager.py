"""test_subscribers_manager.py

Unit tests for SubscriberssManager class.

Copyright 2020 Cervi Robotics
"""

import typing as tp
import unittest
from unittest.mock import MagicMock

import dronekit
from rclpy.node import Node

from px_control.telemetry import Telemetry
from px_control.command_interpreter.subcribers_manager import SubscribersManager
from px_control.mavlink_controller.commands_manager import CommandsManager
from px_control.command_interpreter.subscribers import *


class TestSubscribersManager(unittest.TestCase):
    def setUp(self) -> None:
        self.node = MagicMock(spec=Node)
        self.telemetry = MagicMock(spec=Telemetry)
        self.commands_manager = MagicMock(spec=CommandsManager)

        self.subscribers_manager = SubscribersManager(node=self.node,
                                                      telemetry=self.telemetry,
                                                      commands_manager=self.commands_manager)

    def _test_map_cases(self, cases: tp.List[dict], class_map: dict):
        for case in cases:
            with self.subTest(msg=case):
                self.assertIn(case['name'], class_map)
                self.assertIsInstance(class_map[case['name']], case['instance_of'])

        self.assertEqual(len(cases), len(class_map))

    def test_class_should_have_proper_class_vars_when_initialized(self):
        self.assertIsInstance(self.subscribers_manager._route, dict)
        self.assertIsInstance(self.subscribers_manager._commands, dict)

    def test_class_should_create_map_with_route_subscribers_when_initialized(self):
        cases = [
            {"name": "flight_control_route_normal",
             "instance_of": FlightControlRouteNormal},
            {"name": "flight_control_route_emergency",
             "instance_of": FlightControlRouteEmergency},
            {"name": "flight_control_route_rth",
             "instance_of": FlightControlRouteRTH},
        ]

        self._test_map_cases(cases, self.subscribers_manager._route)

    def test_class_should_create_map_with_command_subscribers_when_initialized(self):
        cases = [
            {"name": "flight_control_land_now",
             "instance_of": FlightControlLandNow},
            {"name": "flight_control_emergency_kill",
             "instance_of": FlightControlEmergencyKill},
            {"name": "flight_control_wait",
             "instance_of": FlightControlWait},
            {"name": "flight_control_continue",
             "instance_of": FlightControlContinue},
            {"name": "flight_control_start_mission",
             "instance_of": FlightControlStartMission},
        ]

        self._test_map_cases(cases, self.subscribers_manager._commands)

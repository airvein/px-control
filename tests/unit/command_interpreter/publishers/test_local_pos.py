"""test_local_pos.py

Unit tests for LocalPos publisher class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import Mock, MagicMock, patch

from rclpy.node import Node

from px_control.common.constants import PublisherNames
from px_control.command_interpreter.publishers import LocalPos
from px_control.telemetry import Telemetry
from drone_types import msg


class TestLocalPos(unittest.TestCase):
    def setUp(self) -> None:
        self.node = MagicMock(spec=Node)
        self.telemetry = MagicMock(spec=Telemetry)

        self.publisher = LocalPos(
            self.node, self.telemetry)

    def test_class_should_create_subscription_when_initialized(self):
        self.node.create_publisher.assert_called_with(
            msg.PxControlLocalPos, PublisherNames.LOCAL_POS)

    @patch('px_control.command_interpreter.publishers.local_pos.get_timestamp')
    def test_publish_should_prepare_message_and_publish_when_called(self, mock_timestamp):
        mock_timestamp.return_value = 1234567890123
        self.telemetry.local_pos = Mock()

        self.publisher.publish()

        self.assertEqual(1234567890123, self.telemetry.local_pos.timestamp)
        self.publisher._publisher.publish.assert_called_with(
            self.telemetry.local_pos
        )

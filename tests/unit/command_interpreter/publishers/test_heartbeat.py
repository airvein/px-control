"""test_heartbeat.py

Unit tests for Heartbeat publisher class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import Mock, patch

from rclpy.node import Node

from px_control.common.constants import PublisherNames
from px_control.command_interpreter.publishers import Heartbeat
from std_msgs import msg


class TestHeartbeat(unittest.TestCase):
    def setUp(self) -> None:
        self.node = Mock(spec=Node)
        self.publisher = Heartbeat(self.node)

    def test_class_should_create_subscription_when_initialized(self):
        self.node.create_publisher.assert_called_with(
            msg.UInt64, PublisherNames.HEARTBEAT)

    @patch('px_control.command_interpreter.publishers.heartbeat.get_timestamp')
    def test_publish_should_publish_heartbeat_when_called(self, mock_timestamp):
        mock_timestamp.return_value = 1234567890123
        message = msg.UInt64(data=mock_timestamp())

        self.publisher.publish()

        self.publisher._publisher.publish.assert_called_with(message)

"""test_send_alert.py

Unit tests for SendAlert publisher class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import Mock, patch

from drone_types.msg import PxControlStatus
from rclpy.node import Node

from px_control.common.alert_codes import PreFlightAlertCodes, \
    EmergencyAlertCodes, RthAlertCodes, NormalAlertCodes, AlertCodes
from px_control.command_interpreter.publishers import SendAlert
from px_control.common.constants import PublisherNames
from px_control.mavlink_controller.mission.emergency_mission import \
    EmergencyMission
from px_control.mavlink_controller.mission.normal_mission import NormalMission
from px_control.mavlink_controller.mission.rth_mission import RthMission
from px_control.telemetry import Telemetry
from drone_types import msg


class TestSendAlert(unittest.TestCase):
    def setUp(self) -> None:
        self.node = Mock(spec=Node)
        self.telemetry = Mock(
            spec=Telemetry, status=Mock(spec=PxControlStatus),
            current_mission=Mock(spec=NormalMission)
        )
        self.publisher = SendAlert(self.node, self.telemetry)

    def test_class_should_create_subscription_when_initialized(self):
        self.node.create_publisher.assert_called_with(
            msg.SendAlert, PublisherNames.SEND_ALERT)

    @patch('px_control.command_interpreter.publishers.send_alert.get_timestamp')
    def test_publish_should_publish_standard_alert_message_when_called(
            self, mock_get_timestamp):
        mock_get_timestamp.return_value = 1234567890123
        alert_name = 'GPS_GLITCH'
        alert = AlertCodes[alert_name]

        self.publisher.publish(alert_name)

        self.publisher._publisher.publish.assert_called_with(
            msg.SendAlert(
                timestamp=1234567890123, code=alert.value)
        )

    @patch('px_control.command_interpreter.publishers.send_alert.get_timestamp')
    @patch.object(SendAlert, '_get_alert_context')
    def test_publish_should_publish_standard_alert_message_when_called(
            self, mock_get_alert_context, mock_get_timestamp):
        mock_get_alert_context.return_value = NormalAlertCodes
        mock_get_timestamp.return_value = 1234567890123
        alert_name = 'UPLOAD_MISSION_FAILED'
        alert = NormalAlertCodes[alert_name]

        self.publisher.publish(alert_name)

        self.publisher._publisher.publish.assert_called_with(
            msg.SendAlert(
                timestamp=1234567890123, code=alert.value)
        )

    @patch('px_control.command_interpreter.publishers.send_alert.type')
    def test_get_alert_context_should_proper_enum_depending_on_drone_state(
            self, mock_type):
        cases = [
            {'flight_state': PxControlStatus.FLIGHT_STATE_ON_GROUND,
             'mission': None, 'enum': PreFlightAlertCodes},
            {'flight_state': PxControlStatus.FLIGHT_STATE_IN_AIR,
             'mission': NormalMission, 'enum': NormalAlertCodes},
            {'flight_state': PxControlStatus.FLIGHT_STATE_IN_AIR,
             'mission': RthMission, 'enum': RthAlertCodes},
            {'flight_state': PxControlStatus.FLIGHT_STATE_IN_AIR,
             'mission': EmergencyMission, 'enum': EmergencyAlertCodes}
        ]

        for case in cases:
            with self.subTest(msg=f'Expected enum: {case["enum"]}'):
                self.telemetry.status.flight_state = case['flight_state']
                mock_type.return_value = case['mission']

                enum = self.publisher._get_alert_context()
                self.assertEqual(case['enum'], enum)

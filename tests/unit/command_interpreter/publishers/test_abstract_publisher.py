"""test_abstract_publisher.py

Unit tests for AbstractPublisher class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from rclpy.node import Node

from px_control.command_interpreter.publishers.abstract_publisher import \
    AbstractPublisher
from px_control.telemetry import Telemetry


class TestAbstractPublisher(unittest.TestCase):
    def setUp(self) -> None:
        self.node = MagicMock(spec=Node)
        self.telemetry = MagicMock(spec=Telemetry)

        self.abstract_publisher = AbstractPublisher(
            self.node, self.telemetry)

    def test_class_should_create_class_objects_when_initialized(self):
        self.assertIsInstance(self.abstract_publisher._node, Node)
        self.assertIsInstance(self.abstract_publisher._telemetry, Telemetry)

    def test_publish_should_raise_NotImplementedError(self):
        with self.assertRaises(NotImplementedError):
            self.abstract_publisher.publish()

import json

rth_route_fixture = json.dumps(
    {
        "route_points": [
            [
                20,
                50.01787769987283,
                21.98469031324652,
                24.0
            ],
            [
                19,
                50.01788670983059,
                21.98428919141277,
                24.0
            ],
            [
                18,
                50.01796850078308,
                21.983663045229438,
                24.0
            ],
            [
                17,
                50.017980649546246,
                21.983637229107707,
                24.0
            ],
            [
                16,
                50.01799508818222,
                21.98361664285419,
                24.0
            ],
            [
                15,
                50.018012540049554,
                21.983595467734318,
                25.0
            ],
            [
                14,
                50.01803076319431,
                21.98357648529187,
                25.0
            ],
            [
                13,
                50.01817199256613,
                21.983543835490853,
                25.0
            ],
            [
                12,
                50.01821451323721,
                21.983542316895456,
                25.0
            ],
            [
                11,
                50.01823248333837,
                21.9835483980554,
                25.0
            ],
            [
                10,
                50.0182570339083,
                21.983576485291866,
                25.0
            ],
            [
                9,
                50.018318537021834,
                21.983682786969577,
                25.0
            ],
            [
                8,
                50.018365613479105,
                21.983786051456498,
                26.0
            ],
            [
                7,
                50.01837197415769,
                21.98383241170386,
                26.0
            ],
            [
                6,
                50.0183663727768,
                21.983953856247744,
                26.0
            ],
            [
                5,
                50.01831686028014,
                21.98425890226908,
                25.0
            ],
            [
                4,
                50.01823563582035,
                21.98462545500784,
                24.0
            ],
            [
                3,
                50.018187294901686,
                21.984794683045113,
                25.0
            ],
            [
                2,
                50.01818920190005,
                21.984901798695162,
                25.0
            ],
            [
                1,
                50.01828,
                21.98508,
                25.0
            ]
        ],
        "zones": {
            "home_approach": 3,
            "home_maneuver": 2,
            "landing_approach": 21,
            "landing_maneuver": 22
        },
        "landing_yaw": 21.37
    }
)

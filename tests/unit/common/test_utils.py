"""test_utils.py

Unit tests for utils module

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import patch

from px_control.common.utils import get_timestamp


class TestUtils(unittest.TestCase):

    @patch('px_control.common.utils.time.time', return_value=1234.123)
    def test_get_timestamp_should_return_proper_value_when_called(self, mock_time):
        expected_output = int(mock_time() * 1000)

        output = get_timestamp()

        self.assertEqual(output, expected_output)

"""test_status_text_parser.py

Unit tests for StatusTextParser class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import Mock

from px_control.common.constants import StatusTextMessages
from px_control.common.status_text_parser import StatusTextParser


class TestStatusTextParser(unittest.TestCase):
    def setUp(self) -> None:
        self.status_test_parser = StatusTextParser()

    def test_status_test_parser_should_create_map_with_patterns(self):
        self.assertIsInstance(self.status_test_parser._patterns_map, dict)
        self.assertEqual(len(StatusTextMessages),
                         len(self.status_test_parser._patterns_map))

    def test_prepare_pattern_should_create_proper_pattern_for_text(self):
        text = Mock(spec=StatusTextMessages, value='Some text')
        expected_pattern = r'^.*SOME.*TEXT.*$'

        pattern = self.status_test_parser._prepare_pattern(text)

        self.assertEqual(expected_pattern, pattern)

    def test_match_should_match_status_text_to_specific_name(self):
        matching_words = [
            'PreArm: GPS Glitch', 'GPS Glitch', 'GPS Glitch 1', 'GPS GLITCH',
            '1 GPS  Glitch', 'CRITICAL: GPS Glitch', 'GPSGlitch'
        ]

        for word in matching_words:
            with self.subTest(msg=f'Expected to match {word}'):
                alert_name = self.status_test_parser.match(word)
                self.assertEqual('GPS_GLITCH', alert_name)

    def test_match_should_return_None_if_match_is_not_found(self):
        not_matching_words = ['GPS', 'Glitch', 'BAD GPS', 'Glitch GPS']

        for word in not_matching_words:
            with self.subTest(msg=f'Expected to not match {word}'):
                alert_name = self.status_test_parser.match(word)
                self.assertIsNone(alert_name)

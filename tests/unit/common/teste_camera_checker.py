from unittest import TestCase
from unittest.mock import Mock, patch

from px_control import settings
from px_control.command_interpreter.publishers_manager import PublishersManager, \
    SendAlert
from px_control.common.camera_checker import CameraChecker
from px_control.common.constants import AlertCodeNames


class TestCameraChecker(TestCase):
    def setUp(self) -> None:
        self.publishers_manager = Mock(
            spec=PublishersManager, send_alert=Mock(spec=SendAlert)
        )
        self.camera_checker = CameraChecker(
            publishers_manager=self.publishers_manager
        )

    @patch.object(CameraChecker, '_stream_available', True)
    @patch.object(CameraChecker, '_camera_available', False)
    def test_check_should_publish_alert_when_camera_is_not_available(self):
        self.camera_checker.check()

        self.publishers_manager.send_alert.publish.assert_called_once_with(
            AlertCodeNames.CAMERA_NOT_CONNECTED.value
        )

    @patch.object(CameraChecker, '_stream_available', False)
    @patch.object(CameraChecker, '_camera_available', True)
    def test_check_should_publish_alert_when_stream_is_not_available(self):
        self.camera_checker.check()

        self.publishers_manager.send_alert.publish.assert_called_once_with(
            AlertCodeNames.STREAM_OFFLINE.value
        )

    @patch('px_control.common.camera_checker.os.listdir')
    def test_camera_available_should_return_camera_status(self, mock_listdir):
        cases = [
            {'listdir_output': [settings.Safety.Camera.CAMERA_NAME],
             'expected_output': True},
            {'listdir_output': ['some_dirs_and_files'],
             'expected_output': False},
        ]

        for case in cases:
            with self.subTest(msg=f"expected_output: {case['expected_output']}"):
                mock_listdir.return_value = case['listdir_output']

                camera_available = self.camera_checker._camera_available

                self.assertEqual(case['expected_output'], camera_available)

    @patch('px_control.common.camera_checker.os.system')
    def test_stream_available_should_return_stream_status(self, mock_os_system):
        cases = [
            {'services': [0, 0], 'expected_output': True},
            {'services': [1, 0], 'expected_output': False},
            {'services': [0, 1], 'expected_output': False},
            {'services': [1, 1], 'expected_output': False},
        ]
        for case in cases:
            with self.subTest(msg=f"expected output: {case['expected_output']}"):
                mock_os_system.side_effect = case['services']

                stream_available = self.camera_checker._stream_available

                self.assertEqual(case['expected_output'], stream_available)

import unittest
from unittest.mock import Mock, call, patch

from rclpy.node import Node

from px_control import settings
from px_control.command_interpreter.publishers import *
from px_control.command_interpreter.publishers_manager import PublishersManager
from px_control.common.camera_checker import CameraChecker
from px_control.common.constants import PublisherNames
from px_control.common.timers import Timers
from px_control.mavlink_controller.commands_manager import CommandsManager


class TestTimers(unittest.TestCase):
    @patch('px_control.common.timers.settings.Safety.Camera.AVAILABLE', True)
    def setUp(self) -> None:
        self.node = Mock(spec=Node)
        self.commands_manager = Mock(spec=CommandsManager)
        self.publishers_manager = Mock(spec=PublishersManager,
                                       heartbeat=Mock(spec=Heartbeat))
        self.publishers_manager.map = self._prepare_publishers_map()
        self.camera_checker = Mock(spec=CameraChecker)

        self.timers = Timers(node=self.node,
                             commands_manager=self.commands_manager,
                             publishers_manager=self.publishers_manager,
                             camera_checker=self.camera_checker)

    def test_class_should_create_timers(self):
        calls = [
            call(1.0 / settings.Timers.TELEMETRY_UPDATE_FREQ,
                 self.timers._telemetry_callback),
            call(1.0 / settings.Timers.COMMANDS_EXECUTE_FREQ,
                 self.timers._commands_executor_callback),
            call(1.0 / settings.Timers.HEARTBEAT_UPDATE_FREQ,
                 self.timers._heartbeat_callback),
            call(1.0 / settings.Timers.CAMERA_CHECKER_UPDATE_FREQ,
                 self.timers._camera_callback),
        ]

        self.node.create_timer.assert_has_calls(calls)

    def test_commands_executor_callback_should_call_process(self):
        self.timers._commands_executor_callback()

        self.commands_manager.process.assert_called_once()

    def test_telemetry_callback_should_all_publishers_messages(self):
        self.timers._telemetry_callback()

        for name, publisher in self.publishers_manager.map.items():
            with self.subTest(msg=f'{name}'):
                publisher.publish.assert_called_once()

    def test_heartbeat_callback_should_publish_heartbeat_message(self):
        self.timers._heartbeat_callback()

        self.publishers_manager.heartbeat.publish.assert_called_once()

    def test_camera_callback_should_call_check_camera(self):
        self.timers._camera_callback()

        self.camera_checker.check.assert_called_once()

    @staticmethod
    def _prepare_publishers_map() -> dict:
        publishers = {
            PublisherNames.ATTITUDE: Mock(spec=Attitude),
            PublisherNames.BATTERY: Mock(spec=Battery),
            PublisherNames.EKF_STATUS: Mock(spec=EKFStatus),
            PublisherNames.GLOBAL_POS: Mock(spec=GlobalPos),
            PublisherNames.IMU: Mock(spec=IMU),
            PublisherNames.LOCAL_POS: Mock(spec=LocalPos),
            PublisherNames.STATUS: Mock(spec=Status),
            PublisherNames.VELOCITY: Mock(spec=Velocity),
        }
        return publishers

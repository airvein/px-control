"""test_telemetry_messages_manager.py

Unit tests for TelemetryMessages class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock, patch, call
import typing as tp

import dronekit

from px_control.command_interpreter.publishers_manager import PublishersManager
from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.telemetry_messages_manager import \
    TelemetryMessagesManager
from px_control.telemetry import Telemetry
from px_control.mavlink_controller.telemetry_messages import *


class TestTelemetryMessagesManager(unittest.TestCase):
    def setUp(self) -> None:
        self.drone = MagicMock(spec=dronekit.Vehicle)
        self.telemetry = MagicMock(spec=Telemetry)
        self.publishers_manager = MagicMock(spec=PublishersManager)

    @patch('px_control.mavlink_controller.telemetry_messages_manager.settings.Connection')
    def test_class_should_create_map_commands(self, mock_settings_connection):
        cases = [
            {'autopilot': None, 'map': self._prepare_common_map()},
            {'autopilot': 'PX4', 'map': self._prepare_px4_map()},
            {'autopilot': 'APM', 'map': self._prepare_apm_map()},
        ]
        for case in cases:
            with self.subTest(msg=case['autopilot']):
                mock_settings_connection.AUTOPILOT = case['autopilot']
                telemetry_messages = TelemetryMessagesManager(
                    telemetry=self.telemetry,
                    drone=self.drone,
                    publishers_manager=self.publishers_manager
                )
                self._test_map_cases(case['map'], telemetry_messages.map)

    def test_class_should_add_drone_message_listeners_when_initialized(self):
        telemetry_messages = TelemetryMessagesManager(
            telemetry=self.telemetry,
            drone=self.drone,
            publishers_manager=self.publishers_manager
        )
        calls = []
        for name, message in telemetry_messages.map.items():
            calls.append(call(name, message.handle))

        self.drone.add_message_listener.assert_has_calls(calls)

    def _test_map_cases(self, cases: tp.List[dict], class_map: dict):
        for case in cases:
            with self.subTest(msg=case):
                self.assertIn(case['name'], class_map)
                self.assertIsInstance(
                    class_map[case['name']], case['instance_of']
                )

    @staticmethod
    def _prepare_common_map() -> tp.List:
        common_map = [
            {"name": TelemetryMessagesNames.HEARTBEAT,
             "instance_of": heartbeat.Heartbeat},
            {"name": TelemetryMessagesNames.SYS_STATUS,
             "instance_of": status.Status},
            {"name": TelemetryMessagesNames.BATTERY_STATUS,
             "instance_of": battery_status.BatteryStatus},
            {"name": TelemetryMessagesNames.ATTITUDE,
             "instance_of": attitude_euler.AttitudeEuler},
            {"name": TelemetryMessagesNames.GLOBAL_POSITION_INT,
             "instance_of": global_position.GlobalPosition},
            {"name": TelemetryMessagesNames.GPS_RAW_INT,
             "instance_of": gps_raw.GpsRaw},
            {"name": TelemetryMessagesNames.LOCAL_POSITION_NED,
             "instance_of": local_position.LocalPosition},
            {"name": TelemetryMessagesNames.COMMAND_ACK,
             "instance_of": command_ack.CommandAck},
            {"name": TelemetryMessagesNames.OTHER,
             "instance_of": other.Other},
            {"name": TelemetryMessagesNames.NAV_CONTROLLER_OUTPUT,
             "instance_of": controller_output.ControllerOutput},
            {"name": TelemetryMessagesNames.MISSION_ACK,
             "instance_of": mission_ack.MissionAck},
            {"name": TelemetryMessagesNames.STATUSTEXT,
             "instance_of": status_text.StatusText},
            {"name": TelemetryMessagesNames.ESTIMATOR_STATUS,
             "instance_of": estimator_status.EstimatorStatus},
            {"name": TelemetryMessagesNames.VIBRATION,
             "instance_of": vibration.Vibration},
        ]
        return common_map

    @staticmethod
    def _prepare_px4_map() -> tp.List:
        px4_map = [
            {"name": TelemetryMessagesNames.ATTITUDE_QUATERNION,
             "instance_of": attitude_quaternion.AttitudeQuaternion},
            {"name": TelemetryMessagesNames.HIGHRES_IMU,
             "instance_of": imu.Imu},
        ]
        return px4_map

    @staticmethod
    def _prepare_apm_map() -> tp.List:
        apm_map = [
            {"name": TelemetryMessagesNames.SCALED_IMU,
             "instance_of": scaled_imu.ScaledImu},
            {"name": TelemetryMessagesNames.EKF_STATUS_REPORT,
             "instance_of": ekf_status.EkfStatus},
            {"name": TelemetryMessagesNames.AHRS2,
             "instance_of": ahrs2.Ahrs2},
        ]
        return apm_map

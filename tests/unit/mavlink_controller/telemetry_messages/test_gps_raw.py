"""test_gps_raw.py

Unit tests for GpsRaw class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.gps_raw import \
    GpsRaw
from px_control.telemetry import Telemetry
from drone_types.msg import PxControlGlobalPos


class TestGpsRaw(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.drone = MagicMock(spec=dronekit.Vehicle)

        self.message = GpsRaw(self.telemetry)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

    def test_handle_should_save_message_data_to_telemetry(self):
        self.telemetry.global_pos = MagicMock(spec=PxControlGlobalPos)
        values = {
            'epv': 1.1, 'eph': 2.2, 'satellites_visible': 3.3,
        }

        message = MagicMock(spec=pm.MAVLink_gps_raw_int_message, **values)

        self.message.handle(
            self.drone, TelemetryMessagesNames.GLOBAL_POSITION_INT, message
        )

        self.assertAlmostEqual(values['epv'], self.telemetry.global_pos.epv)
        self.assertAlmostEqual(values['eph'], self.telemetry.global_pos.eph)
        self.assertAlmostEqual(
            values['satellites_visible'], self.telemetry.global_pos.num_sats
        )

"""test_command_ack.py

Unit tests for CommandAck class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.command_ack import \
    CommandAck
from px_control.telemetry import Telemetry, Ack


class TestCommandAck(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.drone = MagicMock(spec=dronekit.Vehicle)

        self.message = CommandAck(self.telemetry)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

    def test_handle_should_save_message_data_to_telemetry(self):
        self.telemetry.commands_ack = []
        message = MagicMock(spec=pm.MAVLink_command_ack_message)
        message.command = 1
        message.result = 2
        expected_ack = Ack(message.command, message.result)

        self.message.handle(
            self.drone, TelemetryMessagesNames.COMMAND_ACK, message
        )
        ack_message = self.telemetry.commands_ack[0]

        self.assertEqual(1, len(self.telemetry.commands_ack))
        self.assertEqual(expected_ack.command, ack_message.command)
        self.assertEqual(expected_ack.result, ack_message.result)

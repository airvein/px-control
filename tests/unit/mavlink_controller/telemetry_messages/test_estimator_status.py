"""test_estimator_status.py

Unit tests for EstimatorStatus class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.estimator_status import \
    EstimatorStatus
from px_control.telemetry import Telemetry


class TestEstimatorStatus(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)

        self.message = EstimatorStatus(self.telemetry)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

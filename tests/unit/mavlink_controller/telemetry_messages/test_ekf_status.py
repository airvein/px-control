"""test_ekf_status.py

Unit tests for EkfStatus class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.ekf_status import \
    EkfStatus
from px_control.telemetry import Telemetry
from drone_types.msg import PxControlEKFStatus


class TestEkfStatus(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.drone = MagicMock(spec=dronekit.Vehicle)

        self.message = EkfStatus(self.telemetry)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

    def test_handle_should_save_message_data_to_telemetry(self):
        self.telemetry.ekf_status = MagicMock(spec=PxControlEKFStatus)
        values = {
            'flags': 'test_flags', 'velocity_variance': 1.1,
            'pos_horiz_variance': 2.2, 'pos_vert_variance': 3.3,
            'compass_variance': 4.4,
        }
        message = MagicMock(spec=pm.MAVLink_ekf_status_report_message, **values)

        self.message.handle(
            self.drone, TelemetryMessagesNames.EKF_STATUS_REPORT, message
        )

        self.assertEqual('test_flags', self.telemetry.ekf_status.flags)
        self.assertEqual(1.1, self.telemetry.ekf_status.velocity_variance)
        self.assertEqual(2.2, self.telemetry.ekf_status.pos_horiz_variance)
        self.assertEqual(3.3, self.telemetry.ekf_status.pos_vert_variance)
        self.assertEqual(4.4, self.telemetry.ekf_status.compass_variance)

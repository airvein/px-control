"""test_abstract_message.py

Unit tests for AbstractMessage class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.telemetry import Telemetry


class TestAbstractMessage(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)

        self.abstract_message = AbstractMessage(self.telemetry)

    def test_class_should_create_class_objects_when_initialized(self):
        self.assertIsInstance(self.abstract_message._telemetry, Telemetry)

    def test_handle_should_raise_NotImplementedError(self):
        with self.assertRaises(NotImplementedError):
            self.abstract_message.handle()

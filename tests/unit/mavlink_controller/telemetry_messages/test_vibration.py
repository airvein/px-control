"""test_vibration.py

Unit tests for Vibration class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock, patch

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.command_interpreter.publishers import SendAlert
from px_control.command_interpreter.publishers_manager import PublishersManager
from px_control.common.constants import TelemetryMessagesNames, AlertCodeNames
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.vibration import Vibration
from px_control.telemetry import Telemetry


class TestVibration(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.drone = MagicMock(spec=dronekit.Vehicle)
        self.publishers_manager = MagicMock(PublishersManager)
        self.publishers_manager.send_alert = MagicMock(SendAlert)

        self.message = Vibration(self.telemetry, self.publishers_manager)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

    @patch.object(Vibration, '_validate_variance')
    def test_handle_should_validate_variance_and_save_message(
            self, mock_validate_variance):
        message = MagicMock(spec=pm.MAVLink_vibration_message)

        self.message.handle(
            self.drone, TelemetryMessagesNames.VIBRATION, message
        )

        mock_validate_variance.assert_called_once()
        self.assertEqual(self.telemetry.vibration, message)

    @patch('px_control.mavlink_controller.telemetry_messages.vibration.settings.Safety.VIBRATION_TOLERANCE', 30.0)
    def test_validate_variance_should_publish_alert_when_vibration_is_high(
            self):
        cases = [
            {'vibration_x': 35.0, 'vibration_y': 0.0, 'vibration_z': 0.0},
            {'vibration_x': 0.0, 'vibration_y': 35.0, 'vibration_z': 0.0},
            {'vibration_x': 0.0, 'vibration_y': 0.0, 'vibration_z': 35.0},
        ]
        for case in cases:
            with self.subTest(msg=f'case: {cases.index(case)}'):
                msg = MagicMock(spec=pm.MAVLink_vibration_message, **case)

                self.message._validate_variance(msg)

                self.publishers_manager.send_alert.publish.assert_called_once_with(
                    AlertCodeNames.HIGH_VIBE.name
                )
                self.publishers_manager.send_alert.reset_mock()

    @patch('px_control.mavlink_controller.telemetry_messages.vibration.settings.Safety.VIBRATION_TOLERANCE', 30.0)
    def test_validate_variance_should_not_publish_alert_when_vibration_is_low(
            self):
        msg = MagicMock(spec=pm.MAVLink_vibration_message, vibration_x=0.0,
                        vibration_y=0.0, vibration_z=0.0)

        self.message._validate_variance(msg)

        self.publishers_manager.send_alert.publish.assert_not_called()

"""test_status_text.py

Unit tests for StatusText class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock, patch

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.command_interpreter.publishers import SendAlert
from px_control.command_interpreter.publishers_manager import PublishersManager
from px_control.common import constants
from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.status_text import \
    StatusText
from px_control.telemetry import Telemetry


class TestStatusText(unittest.TestCase):
    def setUp(self) -> None:
        self.drone = MagicMock(dronekit.Vehicle)
        self.telemetry = MagicMock(spec=Telemetry)
        self.publishers_manager = MagicMock(spec=PublishersManager)
        self.publishers_manager.send_alert = MagicMock(SendAlert)

        self.message = StatusText(self.telemetry, self.publishers_manager)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

    @patch('px_control.mavlink_controller.telemetry_messages.status_text.StatusTextParser.match')
    def test_message_should_publish_alert_when_text_matches_error_messages(
            self, mock_match):
        mock_match.return_value = 'GPS_GLITCH'
        for item in constants.StatusTextMessages:
            message = MagicMock(
                spec=pm.MAVLink_vibration_message,
                severity=pm.MAV_SEVERITY_ALERT,
                text=item.value
            )
            self.message.handle(
                self.drone, TelemetryMessagesNames.STATUSTEXT, message
            )

            self.publishers_manager.send_alert.publish.assert_called_once_with(
                'GPS_GLITCH'
            )
            self.publishers_manager.send_alert.reset_mock()

"""test_ahrs2.py

Unit tests for Ahrs2 class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.ahrs2 import Ahrs2
from px_control.telemetry import Telemetry


class TestAhrs2(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.drone = MagicMock(spec=dronekit.Vehicle)

        self.message = Ahrs2(self.telemetry)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

    def test_handle_should_save_message_data_to_telemetry(self):
        message = MagicMock(spec=pm.MAVLink_ahrs2_message)

        self.message.handle(self.drone, TelemetryMessagesNames.AHRS2, message)

        self.assertEqual(self.telemetry.ahrs2, message)

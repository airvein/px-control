"""test_mission_ack.py

Unit tests for MissionAck class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import Mock, patch

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.mission.normal_mission import NormalMission
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.mission_ack import \
    MissionAck
from px_control.telemetry import Telemetry


class TestMissionAck(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = Mock(
            spec=Telemetry, current_mission=Mock(spec=NormalMission))
        self.drone = Mock(spec=dronekit.Vehicle)

        self.message = MissionAck(self.telemetry)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

    @patch('px_control.mavlink_controller.telemetry_messages.mission_ack.os.environ.get')
    def test_handle_should_save_mission_ack_message_when_mission_is_not_None(
            self, mock_environ_get):
        message = Mock(spec=pm.MAVLink_mission_ack_message,
                       mission_type=pm.MAV_MISSION_TYPE_MISSION,
                       type=pm.MAV_MISSION_DENIED)
        cases = [
            {'sitl': '0', 'mission_type': pm.MAV_MISSION_TYPE_MISSION,
             'result': pm.MAV_MISSION_DENIED},
            {'sitl': '1', 'mission_type': pm.MAV_MISSION_TYPE_MISSION,
             'result': pm.MAV_MISSION_ACCEPTED},
        ]
        for case in cases:
            with self.subTest(msg=f"STIL: {case['sitl']}"):
                mock_environ_get.return_value = case['sitl']
                self.message.handle(
                    self.drone, TelemetryMessagesNames.MISSION_ACK, message
                )

                ack = self.telemetry.current_mission.mission_ack
                self.assertEqual(case['mission_type'], ack.mission_type)
                self.assertEqual(case['result'], ack.result)

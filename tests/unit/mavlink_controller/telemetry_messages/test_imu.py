"""test_imu.py

Unit tests for Imu class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.imu import Imu
from px_control.telemetry import Telemetry
from drone_types import msg


class TestImu(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.drone = MagicMock(spec=dronekit.Vehicle)

        self.message = Imu(self.telemetry)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

    def test_handle_should_save_message_data_to_telemetry(self):
        self.telemetry.imu = MagicMock(spec=msg.PxControlIMU)
        values = {
            'acc': msg.Vector3f(x=1.1, y=1.2, z=1.3),
            'gyro': msg.Vector3f(x=2.1, y=2.2, z=2.3),
            'mag': msg.Vector3f(x=3.1, y=3.2, z=3.3),
        }

        message = self.prepare_imu_message(values)

        self.message.handle(
            self.drone, TelemetryMessagesNames.HIGHRES_IMU, message
        )

        self.assertAlmostEqualVector3f(values['acc'], self.telemetry.imu.acc)
        self.assertAlmostEqualVector3f(values['gyro'], self.telemetry.imu.gyro)
        self.assertAlmostEqualVector3f(values['mag'], self.telemetry.imu.mag)

    @staticmethod
    def prepare_imu_message(imu_values: dict):
        imu_multiplier = 1000

        message = MagicMock(spec=pm.MAVLink_highres_imu_message)
        message.xacc = imu_values['acc'].x * imu_multiplier
        message.yacc = imu_values['acc'].y * imu_multiplier
        message.zacc = imu_values['acc'].z * imu_multiplier
        message.xgyro = imu_values['gyro'].x * imu_multiplier
        message.ygyro = imu_values['gyro'].y * imu_multiplier
        message.zgyro = imu_values['gyro'].z * imu_multiplier
        message.xmag = imu_values['mag'].x * imu_multiplier
        message.ymag = imu_values['mag'].y * imu_multiplier
        message.zmag = imu_values['mag'].z * imu_multiplier

        return message

    def assertAlmostEqualVector3f(self, first, second, places=None, msg=None,
                                  delta=None):
        self.assertAlmostEqual(first.x, second.x, places, msg, delta)
        self.assertAlmostEqual(first.y, second.y, places, msg, delta)
        self.assertAlmostEqual(first.z, second.z, places, msg, delta)

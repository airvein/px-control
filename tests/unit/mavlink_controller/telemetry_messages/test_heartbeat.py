"""test_heartbeat.py

Unit tests for Heartbeat class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock, patch

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.mission.normal_mission import NormalMission
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.heartbeat import \
    Heartbeat
from px_control.telemetry import Telemetry
from drone_types.msg import PxControlStatus


class TestHeartbeat(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.telemetry.status = MagicMock(spec=PxControlStatus)
        self.drone = MagicMock(spec=dronekit.Vehicle)

        self.message = Heartbeat(self.telemetry)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

    @patch.object(Heartbeat, '_process_system_status')
    @patch.object(Heartbeat, '_process_drone_arm_state')
    def test_handle_should_process_system_status_and_drone_arm_state(
            self, mock_process_drone_arm_state, mock_process_system_status):
        message = MagicMock(spec=pm.MAVLink_heartbeat_message)

        self.message.handle(
            self.drone, TelemetryMessagesNames.HEARTBEAT, message
        )

        mock_process_system_status.assert_called_once()
        mock_process_drone_arm_state.assert_called_once()

    @patch.object(Heartbeat, '_is_in_takeoff')
    @patch.object(Heartbeat, '_is_in_land')
    def test_process_system_status_should_set_flight_state(
            self, mock__is_in_land, mock_is_in_takeoff):
        cases = self.prepare_flight_state_cases()
        message = MagicMock(spec=pm.MAVLink_heartbeat_message)

        for case in cases:
            with self.subTest(msg=case['flight_state']):
                message.system_status = case['status']
                self.telemetry.current_mission = case['mission']
                mock_is_in_takeoff.return_value = case['takeoff']
                mock__is_in_land.return_value = case['landing']

                self.message._process_system_status(message, self.drone)

                self.assertEqual(
                    case['flight_state'], self.telemetry.status.flight_state
                )

    def test_process_drone_arm_state_should_set_arm_state(self):
        cases = [
            {'armed': True, 'armable': False,
             'arm_state': PxControlStatus.ARM_STATE_ARMED},
            {'armed': False, 'armable': True,
             'arm_state': PxControlStatus.ARM_STATE_READY},
            {'armed': False, 'armable': False,
             'arm_state': PxControlStatus.ARM_STATE_NOT_READY}
        ]

        for case in cases:
            with self.subTest(msg=case['arm_state']):
                self.drone.armed = case['armed']
                self.drone.is_armable = case['armable']

                self.message._process_drone_arm_state(self.drone)

                self.assertEqual(
                    case['arm_state'], self.telemetry.status.arm_state
                )

    def test_is_in_takeoff_should_return_True_when_drone_is_in_takeoff(self):
        self.telemetry.current_mission = MagicMock(spec=NormalMission)
        self.drone.commands = MagicMock(spec=dronekit.CommandSequence)
        cases = [
            {'mission_index': 1, 'commands_index': 1, 'output': True},
            {'mission_index': 1, 'commands_index': 2, 'output': False},
            {'mission_index': None, 'commands_index': 1, 'output': False},
        ]
        for case in cases:
            with self.subTest(msg=case['mission_index']):
                self.telemetry.current_mission.takeoff_index = case['mission_index']
                self.drone.commands.next = case['commands_index']

                output = self.message._is_in_takeoff(self.drone)

                self.assertEqual(case['output'], output)

    def test_is_in_land_should_return_True_when_drone_is_landing(self):
        self.telemetry.current_mission = MagicMock(spec=NormalMission)
        self.drone.commands = MagicMock(spec=dronekit.CommandSequence)
        cases = [
            {'mission_index': 1, 'commands_index': 1, 'output': True},
            {'mission_index': 1, 'commands_index': 2, 'output': False},
            {'mission_index': None, 'commands_index': 1, 'output': False},
        ]
        for case in cases:
            with self.subTest(msg=case['mission_index']):
                self.telemetry.current_mission.landing_index = case['mission_index']
                self.drone.commands.next = case['commands_index']

                output = self.message._is_in_land(self.drone)

                self.assertEqual(case['output'], output)

    @staticmethod
    def prepare_flight_state_cases():
        cases = [
            {
                'status': pm.MAV_STATE_STANDBY,
                'mission': None,
                'takeoff': False,
                'landing': False,
                'flight_state': PxControlStatus.FLIGHT_STATE_ON_GROUND
            },
            {
                'status': pm.MAV_STATE_ACTIVE,
                'mission': None,
                'takeoff': False,
                'landing': False,
                'flight_state': PxControlStatus.FLIGHT_STATE_IN_AIR
            },
            {
                'status': pm.MAV_STATE_ACTIVE,
                'mission': MagicMock(spec=NormalMission),
                'takeoff': True,
                'landing': False,
                'flight_state': PxControlStatus.FLIGHT_STATE_TAKEOFF
            },
            {
                'status': pm.MAV_STATE_ACTIVE,
                'mission': MagicMock(spec=NormalMission),
                'takeoff': False,
                'landing': True,
                'flight_state': PxControlStatus.FLIGHT_STATE_LANDING
            }
        ]
        return cases

"""test_other.py

Unit tests for Other class.

Copyright 2020 Cervi Robotics
"""
import logging
import unittest
from unittest.mock import MagicMock, patch

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.other import Other
from px_control.telemetry import Telemetry


class TestOther(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.drone = MagicMock(spec=dronekit.Vehicle)
        patch_logger = patch(
            'px_control.mavlink_controller.telemetry_messages.other.logger',
            spec=logging.Logger
        )
        self.logger = patch_logger.start()
        self.addCleanup(patch_logger.stop)

        self.message = Other(self.telemetry)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

    def test_handle_should_save_message_data_to_telemetry(self):
        message = MagicMock(spec=pm.MAVLink_message)
        message.get_type.return_value = 'test_type'

        self.message.handle(self.drone, TelemetryMessagesNames.OTHER, message)

        self.logger.warning.assert_called_once_with(
            f"Got unexpected msg: '{message.get_type()}'"
        )

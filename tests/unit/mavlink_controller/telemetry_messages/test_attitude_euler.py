"""test_attitude_euler.py

Unit tests for AltitudeEuler class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock, patch

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm
from transforms3d.euler import euler2quat

from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.attitude_euler import \
    AttitudeEuler
from px_control.telemetry import Telemetry
from drone_types.msg import PxControlAttitude


class TestAltitudeEuler(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.drone = MagicMock(spec=dronekit.Vehicle)

        self.message = AttitudeEuler(self.telemetry)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

    @patch('px_control.mavlink_controller.telemetry_messages.attitude_euler.settings.Connection')
    def test_handle_should_save_message_data_to_telemetry(
            self, mock_settings_connection):
        mock_settings_connection.AUTOPILOT = 'APM'
        self.telemetry.attitude = MagicMock(spec=PxControlAttitude)

        values = {
            'rollspeed': 1.1, 'pitchspeed': 2.2, 'yawspeed': 3.3,
            'quaternion': euler2quat(1.1, 2.2, 3.3, 'rxyz'),
        }

        message = MagicMock(spec=pm.MAVLink_attitude_message)
        message.rollspeed = values['rollspeed']
        message.pitchspeed = values['pitchspeed']
        message.yawspeed = values['yawspeed']

        self.message.handle(
            self.drone, TelemetryMessagesNames.ATTITUDE, message
        )

        self.assertEqual(values['quaternion'][0], self.telemetry.attitude.q_w)
        self.assertEqual(values['quaternion'][1], self.telemetry.attitude.q_x)
        self.assertEqual(values['quaternion'][2], self.telemetry.attitude.q_y)
        self.assertEqual(values['quaternion'][3], self.telemetry.attitude.q_z)
        self.assertEqual(
            values['rollspeed'], self.telemetry.attitude.roll_speed
        )
        self.assertEqual(
            values['pitchspeed'], self.telemetry.attitude.pitch_speed
        )
        self.assertEqual(values['yawspeed'], self.telemetry.attitude.yaw_speed)

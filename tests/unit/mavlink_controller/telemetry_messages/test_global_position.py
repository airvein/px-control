"""test_global_position.py

Unit tests for GlobalPosition class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.global_position import \
    GlobalPosition
from px_control.telemetry import Telemetry
from drone_types.msg import PxControlGlobalPos


class TestGlobalPosition(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.drone = MagicMock(spec=dronekit.Vehicle)

        self.message = GlobalPosition(self.telemetry)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

    def test_handle_should_save_message_data_to_telemetry(self):
        self.telemetry.global_pos = MagicMock(spec=PxControlGlobalPos)
        values = {'lat': 1.1, 'lon': 2.2, 'alt': 3.3}

        message = MagicMock(spec=pm.MAVLink_gps_raw_int_message)
        message.lat = values['lat'] * 1e7
        message.lon = values['lon'] * 1e7
        message.alt = values['alt'] * 1000.0

        self.message.handle(
            self.drone, TelemetryMessagesNames.GLOBAL_POSITION_INT, message
        )

        self.assertAlmostEqual(
            values['lat'], self.telemetry.global_pos.latitude
        )
        self.assertAlmostEqual(
            values['lon'], self.telemetry.global_pos.longitude
        )
        self.assertAlmostEqual(
            values['alt'], self.telemetry.global_pos.altitude
        )

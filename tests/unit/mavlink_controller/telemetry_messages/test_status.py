"""test_status.py

Unit tests for Status class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock, patch

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.status import Status
from px_control.telemetry import Telemetry
from drone_types.msg import PxControlStatus, PxControlBattery


class TestStatus(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.drone = MagicMock(spec=dronekit.Vehicle)

        self.message = Status(self.telemetry)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

    @patch('px_control.mavlink_controller.telemetry_messages.status.settings.Connection')
    def test_handle_should_save_message_data_to_telemetry(
            self, mock_settings_connection):
        mock_settings_connection.AUTOPILOT = 'APM'
        self.telemetry.status = MagicMock(spec=PxControlStatus)
        self.telemetry.battery = MagicMock(spec=PxControlBattery)

        values = {
            'load': 100.0, 'voltage': 1500.0, 'current': 21.37,
            'expected_voltage': 1500.0 / 1000.0,
            'expected_current': 21.37 / 100.0,
            'remaining': 51, 'expected_remaining': 51.0,
            'mode': 'test_mode'
        }

        self.drone.mode.name = values['mode']
        message = MagicMock(spec=pm.MAVLink_sys_status_message)
        message.voltage_battery = values['voltage']
        message.current_battery = values['current']
        message.battery_remaining = values['remaining']

        self.message.handle(
            self.drone, TelemetryMessagesNames.SYS_STATUS, message
        )

        self.assertEqual(
            values['expected_voltage'], self.telemetry.battery.voltage
        )
        self.assertEqual(
            values['expected_current'], self.telemetry.battery.current
        )
        self.assertEqual(
            values['expected_remaining'], self.telemetry.battery.remaining
        )
        self.assertEqual(values['mode'], self.telemetry.status.flight_mode)

"""test_attitude_quaternion.py

Unit tests for AttitudeQuaternion class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.attitude_quaternion import \
    AttitudeQuaternion
from px_control.telemetry import Telemetry
from drone_types.msg import PxControlAttitude


class TestAttitudeQuaternion(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.drone = MagicMock(spec=dronekit.Vehicle)

        self.message = AttitudeQuaternion(self.telemetry)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

    def test_handle_should_save_message_data_to_telemetry(self):
        self.telemetry.attitude = MagicMock(spec=PxControlAttitude)
        values = {
            'q1': 1.1, 'q2': 2.2, 'q3': 3.3, 'q4': 4.4,
            'rollspeed': 5.5, 'pitchspeed': 6.6, 'yawspeed': 7.7,
        }

        message = MagicMock(
            spec=pm.MAVLink_attitude_quaternion_message, **values
        )

        self.message.handle(
            self.drone, TelemetryMessagesNames.ATTITUDE_QUATERNION, message
        )

        self.assertEqual(values['q1'], self.telemetry.attitude.q_w)
        self.assertEqual(values['q2'], self.telemetry.attitude.q_x)
        self.assertEqual(values['q3'], self.telemetry.attitude.q_y)
        self.assertEqual(values['q4'], self.telemetry.attitude.q_z)
        self.assertEqual(
            values['rollspeed'], self.telemetry.attitude.roll_speed
        )
        self.assertEqual(
            values['pitchspeed'], self.telemetry.attitude.pitch_speed
        )
        self.assertEqual(
            values['yawspeed'], self.telemetry.attitude.yaw_speed
        )

"""test_local_position.py

Unit tests for LocalPosition class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.local_position import \
    LocalPosition
from px_control.telemetry import Telemetry
from drone_types.msg import PxControlLocalPos, PxControlVelocity


class TestLocalPosition(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.drone = MagicMock(spec=dronekit.Vehicle)

        self.message = LocalPosition(self.telemetry)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

    def test_handle_should_save_message_data_to_telemetry(self):
        self.telemetry.local_pos = MagicMock(spec=PxControlLocalPos)
        self.telemetry.velocity = MagicMock(spec=PxControlVelocity)
        values = {
            'x': 1.1, 'y': 2.2, 'z': 3.3, 'vx': 1.1, 'vy': 2.2, 'vz': 3.3,
        }
        message = MagicMock(
            spec=pm.MAVLink_local_position_ned_message, **values
        )

        self.message.handle(
            self.drone, TelemetryMessagesNames.LOCAL_POSITION_NED, message
        )

        self.assertAlmostEqual(values['x'], self.telemetry.local_pos.x)
        self.assertAlmostEqual(values['y'], self.telemetry.local_pos.y)
        self.assertAlmostEqual(values['z'], self.telemetry.local_pos.z)
        self.assertAlmostEqual(values['vx'], self.telemetry.velocity.vx)
        self.assertAlmostEqual(values['vy'], self.telemetry.velocity.vy)
        self.assertAlmostEqual(values['vz'], self.telemetry.velocity.vz)

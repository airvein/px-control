"""test_battery_status.py

Unit tests for BatteryStatus class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock, patch

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.mavlink_controller.telemetry_messages.battery_status import \
    BatteryStatus
from px_control.telemetry import Telemetry
from drone_types.msg import PxControlBattery


class TestBatteryStatus(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.drone = MagicMock(spec=dronekit.Vehicle)

        self.message = BatteryStatus(self.telemetry)

    def test_message_should_inherit_from_abstract_message(self):
        self.assertIsInstance(self.message, AbstractMessage)

    @patch('px_control.mavlink_controller.telemetry_messages.battery_status.settings.Connection')
    def test_handle_should_save_message_data_to_telemetry(
            self, mock_settings_connection):
        self.telemetry.battery = MagicMock(spec=PxControlBattery)
        mock_settings_connection.AUTOPILOT = 'PX4'
        values = {
            'voltage': [1100, 2200, 3300, 65535],
            'expected_voltage': sum([1.1, 2.2, 3.3]),
            'current': 21.37, 'expected_current': 21.37 / 100.0,
            'current_consumed': 1100.0,
            'expected_current_consumed': 1100.0 / 1000.0,
            'percent': 51.9, 'expected_percent': int(51.9),
        }

        message = MagicMock(spec=pm.MAVLink_sys_status_message)
        message.voltages = values['voltage']
        message.current_battery = values['current']
        message.current_consumed = values['current_consumed']
        message.battery_remaining = values['percent']

        self.message.handle(
            self.drone, TelemetryMessagesNames.BATTERY_STATUS, message
        )

        self.assertEqual(
            values['expected_voltage'], self.telemetry.battery.voltage
        )
        self.assertEqual(
            values['expected_current'], self.telemetry.battery.current
        )
        self.assertEqual(values['expected_current_consumed'],
                         self.telemetry.battery.current_consumed)
        self.assertEqual(
            values['expected_percent'], self.telemetry.battery.percent
        )

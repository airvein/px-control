"""test_commands_manager.py

Unit tests for CommandsManager class.

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import MagicMock, patch
from queue import Queue

import dronekit

from px_control.command_interpreter.publishers import SendAlert
from px_control.command_interpreter.publishers_manager import PublishersManager
from px_control.common.constants import CommandNames, AlertCodeNames
from px_control.common.pending_command import PendingCommand
from px_control.mavlink_controller.ack_processor import AckProcessor, AckStatus
from px_control.mavlink_controller.commands import *
from px_control.mavlink_controller.commands_manager import CommandsManager
from px_control.mavlink_controller.mission_ack_processor import \
    MissionAckProcessor
from px_control.telemetry import Telemetry


class TestPendingCommand(unittest.TestCase):
    def test_class_should_have_proper_class_vars_when_initialized(self):
        expected_arguments = {
            'args': ['arg1', 'arg2'],
            'kwargs': {'kwarg1': 1, 'kwarg2': 2},
        }
        expected_command = MagicMock()

        self.pending_command = PendingCommand(expected_command,
                                              *expected_arguments['args'],
                                              **expected_arguments['kwargs'])

        self.assertEqual(expected_command, self.pending_command.command)
        self.assertEqual(
            tuple(expected_arguments['args']), self.pending_command.args
        )
        self.assertEqual(
            expected_arguments['kwargs'], self.pending_command.kwargs
        )


class TestCommandsManager(unittest.TestCase):
    def setUp(self) -> None:
        self.drone = MagicMock(spec=dronekit.Vehicle)
        self.telemetry = MagicMock(spec=Telemetry)
        self.publishers_manager = MagicMock(
            spec=PublishersManager, send_alert=MagicMock(spec=SendAlert)
        )
        self.commands_manager = CommandsManager(drone=self.drone,
                                                telemetry=self.telemetry,
                                                publishers_manager=self.publishers_manager)

    def test_class_should_have_proper_class_vars_when_initialized(self):
        self.assertIsInstance(self.commands_manager._drone, dronekit.Vehicle)
        self.assertIsInstance(self.commands_manager._telemetry, Telemetry)
        self.assertIsInstance(self.commands_manager._publishers_manager, PublishersManager)
        self.assertIsInstance(self.commands_manager._queue, Queue)

    def test_class_should_create_map_with_command_instances_when_initialized(
            self):
        cases = [
            {"name": CommandNames.ARM,
             "instance_of": Arm},
            {"name": CommandNames.SET_GROUNDSPEED,
             "instance_of": SetGroundspeed},
            {"name": CommandNames.SET_MODE,
             "instance_of": SetMode},
            {"name": CommandNames.TAKEOFF,
             "instance_of": Takeoff},
            {"name": CommandNames.TERMINATE_FLIGHT,
             "instance_of": TerminateFlight},
            {"name": CommandNames.START_MISSION,
             "instance_of": StartMission},
            {"name": CommandNames.UPLOAD_MISSION,
             "instance_of": UploadMission}
        ]

        for case in cases:
            with self.subTest(msg=case):
                self.assertIn(case['name'], self.commands_manager._map)
                self.assertIsInstance(self.commands_manager._map[case['name']],
                                      case['instance_of'])

        self.assertEqual(len(cases), len(self.commands_manager._map))

    @patch('px_control.mavlink_controller.commands_manager.PendingCommand')
    def test_add_should_prepare_and_and_proper_command(self, mock_pending_command):
        mock_pending_command.return_value = 'test_command'
        expected_command = {
            'name': 'arm',
            'args': ['arg1', 'arg2'],
            'kwargs': {'kwarg1': 1, 'kwarg2': 2},
        }

        self.commands_manager.add(expected_command['name'],
                                  *expected_command['args'],
                                  **expected_command['kwargs'])

        mock_pending_command.assert_called_once_with(
            self.commands_manager._map[expected_command['name']],
            *expected_command['args'],
            **expected_command['kwargs'])

        self.assertEqual(1, self.commands_manager._queue.qsize())
        self.assertEqual(
            'test_command', self.commands_manager._queue.get_nowait()
        )

    @patch.object(CommandsManager, '_process_command')
    def test_process_should_process_command_when_ack_processor_is_None(
            self, mock_process_command):
        self.commands_manager._ack_processor = None

        self.commands_manager.process()

        mock_process_command.assert_called_once()

    @patch.object(CommandsManager, '_process_ack')
    def test_process_should_process_ack_command_when_ack_processor_is_not_None(
            self, mock_process_ack):
        self.commands_manager._ack_processor = MagicMock(spec=AckProcessor)

        self.commands_manager.process()

        mock_process_ack.assert_called_once()

    @patch('px_control.mavlink_controller.commands_manager.Queue.empty')
    @patch('px_control.mavlink_controller.commands_manager.Queue.get_nowait')
    @patch.object(CommandsManager, '_create_ack_processor')
    def test_process_command_should_process_command_and_create_ack_processor_when_queue_not_empty(
            self, mock_crete_ack_processor, mock_queue_get_nowait,
            mock_queue_empty):
        mock_queue_empty.return_value = False
        pending_command = self.prepare_pending_command()
        mock_queue_get_nowait.return_value = pending_command

        self.commands_manager._process_command()

        pending_command.command.handle.assert_called_once_with(
            *pending_command.args, **pending_command.kwargs
        )
        mock_crete_ack_processor.assert_called_once_with(pending_command)

    def test_process_ack_should_process_ack_and_set_ack_processor_to_None_when_SUCCES(
            self):
        mock_ack_processor = MagicMock(spec=AckProcessor)
        mock_ack_processor.status = AckStatus.SUCCESS
        self.commands_manager._ack_processor = mock_ack_processor

        self.commands_manager._process_ack()

        mock_ack_processor.process.assert_called_once()
        self.assertIsNone(self.commands_manager._ack_processor)

    @patch.object(CommandsManager, '_send_alert')
    @patch.object(CommandsManager, '_clear_queue')
    def test_process_ack_should_process_ack_send_alert_clear_queue_and_set_ack_processor_to_None_when_FAILED(
            self, mock_clear_queue, mock_send_alert):
        mock_ack_processor = MagicMock(
            spec=AckProcessor, pending_command=MagicMock(spec=PendingCommand)
        )
        mock_ack_processor.status = AckStatus.FAILED
        self.commands_manager._ack_processor = mock_ack_processor

        self.commands_manager._process_ack()

        mock_ack_processor.process.assert_called_once()
        mock_send_alert.assert_called_once_with(
            mock_ack_processor.pending_command
        )
        mock_clear_queue.assert_called_once()
        self.assertIsNone(self.commands_manager._ack_processor)

    def test_clear_queue_should_clear_queue(self):
        self.commands_manager._queue.put_nowait(MagicMock())

        self.commands_manager._clear_queue()

        self.assertTrue(self.commands_manager._queue.empty())

    def test_crete_ack_processor_should_create_mission_ack_processor_when_command_is_upload_mission(
            self):
        pending_command = MagicMock(
            spec=PendingCommand, command=MagicMock(spec=UploadMission)
        )

        self.commands_manager._create_ack_processor(pending_command)

        self.assertIsInstance(
            self.commands_manager._ack_processor, MissionAckProcessor
        )

    def test_create_ack_processor_should_create_ack_processor_when_command_is_not_upload_mission(
            self):
        pending_command = MagicMock(
            spec=PendingCommand, command=MagicMock(spec=Arm)
        )

        self.commands_manager._create_ack_processor(pending_command)

        self.assertIsInstance(
            self.commands_manager._ack_processor, AckProcessor
        )

    def test_send_alert_should_send_alert_related_to_command(self):
        cases = [
            {'command': MagicMock(spec=StartMission),
             'code': AlertCodeNames.START_MISSION_FAILED},
            {'command': MagicMock(spec=UploadMission),
             'code': AlertCodeNames.UPLOAD_MISSION_FAILED},
            {'command': MagicMock(spec=Arm),
             'code': AlertCodeNames.UNABLE_TO_ARM_DRONE},
            {'command': MagicMock(spec=SetMode),
             'code': AlertCodeNames.UNABLE_TO_CHANGE_MODE},
        ]
        for case in cases:
            with self.subTest(msg=f'Command {case["command"]}'):
                pending_command = MagicMock(
                    spec=PendingCommand, command=case['command']
                )

                self.commands_manager._send_alert(pending_command)

                self.publishers_manager.send_alert.publish.assert_called_once_with(
                    case['code'].name)
                self.publishers_manager.send_alert.publish.reset_mock()

    def test_send_alert_should_not_send_alert_when_command_is_not_in_map(self):
        commands = [MagicMock(spec=Takeoff), MagicMock(spec=Takeoff),
                    MagicMock(spec=SetGroundspeed)]
        for command in commands:
            with self.subTest(msg=f'Command {command}'):
                pending_command = MagicMock(
                    spec=PendingCommand, command=command
                )

                self.commands_manager._send_alert(pending_command)

                self.publishers_manager.send_alert.publish.assert_not_called()

    @staticmethod
    def prepare_pending_command():
        command_values = {
            'command': MagicMock(spec=Arm),
            'args': ['arg1', 'arg2'],
            'kwargs': {'kwarg1': 1, 'kwarg2': 2},
        }
        pending_command = MagicMock(spec=PendingCommand)
        pending_command.command = command_values['command']
        pending_command.args = command_values['args']
        pending_command.kwargs = command_values['kwargs']

        return pending_command

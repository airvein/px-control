"""test_mission_ack_processor.py

Unit tests for MissionAckProcessor class.

Copyright 2020 Cervi Robotics
"""
import logging
import unittest
from unittest.mock import MagicMock, patch, Mock

from drone_types.msg import PxControlStatus
from pymavlink.mavutil import mavlink

from px_control.mavlink_controller.mission.emergency_mission import EmergencyMission
from px_control.mavlink_controller.mission.normal_mission import NormalMission
from px_control.mavlink_controller.mission.rth_mission import RthMission
from px_control.mavlink_controller.mission_ack_processor import \
    MissionAckProcessor, AckStatus
from px_control.mavlink_controller.commands import *
from px_control.mavlink_controller.commands_manager import PendingCommand
from px_control.telemetry import Telemetry, MissionAck


class TestMissionAckProcessor(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(
            spec=Telemetry, current_mission=MagicMock(spec=NormalMission)
        )
        self.pending_command = self.prepare_pending_command()
        patch_logger = patch(
            'px_control.mavlink_controller.mission_ack_processor.logger',
            spec=logging.Logger
        )
        self.logger = patch_logger.start()
        self.addCleanup(patch_logger.stop)

        self.mission_ack_processor = MissionAckProcessor(
            telemetry=self.telemetry, pending_command=self.pending_command
        )

    def test_class_should_have_proper_class_vars_when_initialized(self):
        self.assertIsInstance(
            self.mission_ack_processor._telemetry, Telemetry
        )
        self.assertIsInstance(
            self.mission_ack_processor.pending_command, PendingCommand
        )

    def test_repeat_mission_should_call_command_handler_with_proper_arguments(
            self):
        self.mission_ack_processor._repeat_mission()

        self.pending_command.command.handle.assert_called_once_with(
            *self.pending_command.args, **self.pending_command.kwargs
        )

    @patch.object(MissionAckProcessor, '_check_ack')
    @patch.object(MissionAckProcessor, '_ack_accepted')
    @patch.object(MissionAckProcessor, '_ack_rejected')
    @patch.object(MissionAckProcessor, '_ack_failed')
    def test_process_should_call_proper_ack_handler_when_status_is_PROCESSING_and_mission_is_not_uploading(
            self, _ack_rejected, _ack_accepted, _add_ack, _ack_failed):
        self.mission_ack_processor.status = AckStatus.PROCESSING
        self.telemetry.current_mission.uploading = False
        mocked_ack_map = {
            None: _add_ack,
            mavlink.MAV_RESULT_ACCEPTED: _ack_accepted,
            mavlink.MAV_MISSION_OPERATION_CANCELLED: _ack_rejected,
            mavlink.MAV_MISSION_INVALID_SEQUENCE: _ack_rejected,
            mavlink.MAV_MISSION_DENIED: _ack_rejected,
            'OTHER': _ack_failed

        }
        self.mission_ack_processor._ack_map = mocked_ack_map

        for result, method in mocked_ack_map.items():
            with self.subTest(result):
                self.mission_ack_processor.result = result
                self.mission_ack_processor.process()

                mocked_ack_map[result].assert_called_once()
                mocked_ack_map[result].reset_mock()

    @patch.object(MissionAckProcessor, '_get_result')
    def test_check_ack_should_cal_get_result_and_decrement_counter_when_counter_is_greater_than_zero(
            self, mock_get_result):
        self.mission_ack_processor.check_counter = 3

        self.mission_ack_processor._check_ack()

        mock_get_result.assert_called_once()
        self.assertEqual(2, self.mission_ack_processor.check_counter)

    def test_check_ack_should_set_status_FAILED_and_log_warning_when_counter_is_not_greater_than_zero(
            self):
        self.mission_ack_processor.check_counter = 0
        self.mission_ack_processor._check_ack()

        self.assertEqual(AckStatus.FAILED, self.mission_ack_processor.status)
        self.logger.warning.assert_called_once_with(f'NOT RECEIVED MISSION ACK')

    def test_get_result_should_save_ack_result(self):
        self.telemetry.current_mission.mission_ack = MissionAck(
            mavlink.MAV_MISSION_TYPE_MISSION, mavlink.MAV_MISSION_ACCEPTED
        )

        self.mission_ack_processor._get_result()

        self.assertEqual(
            self.telemetry.current_mission.mission_ack.result,
            self.mission_ack_processor.result
        )

    def test_ack_accepted_should_set_status_SUCCESS_and_update_mission_state(
            self):
        self.telemetry.status = MagicMock(spec=PxControlStatus)
        cases = [
            {'mission': Mock(spec=NormalMission),
             'state': PxControlStatus.MISSION_STATE_NORMAL},
            {'mission': Mock(spec=RthMission),
             'state': PxControlStatus.MISSION_STATE_RTH},
            {'mission': Mock(spec=EmergencyMission),
             'state': PxControlStatus.MISSION_STATE_EMERGENCY},
        ]
        for case in cases:
            with self.subTest(msg=f"mission_status: {case['state']}"):
                self.telemetry.current_mission = case['mission']

                self.mission_ack_processor._ack_accepted()

                self.assertEqual(
                    AckStatus.SUCCESS, self.mission_ack_processor.status)
                self.assertEqual(
                    case['state'], self.telemetry.status.mission_state)

    @patch.object(MissionAckProcessor, '_repeat_mission')
    def test_ack_rejected_should_repeat_command_and_set_result_to_None_when_counter_is_greater_than_zero(
            self, mock_repeat_command):
        self.mission_ack_processor.repeat_counter = 2

        self.mission_ack_processor._ack_rejected()

        self.assertIsNone(self.mission_ack_processor.result)
        mock_repeat_command.assert_called_once()
        self.assertEqual(1, self.mission_ack_processor.repeat_counter)

    def test_ack__rejected_should_set_status_FAILED_when_counter_is_not_greater_than_zero(
            self):
        self.mission_ack_processor.repeat_counter = 0

        self.mission_ack_processor._ack_rejected()

        self.assertEqual(AckStatus.FAILED, self.mission_ack_processor.status)

    def test_ack_failed_should_set_status_FAILED(self):
        self.mission_ack_processor._ack_failed()

        self.assertEqual(AckStatus.FAILED, self.mission_ack_processor.status)

    @staticmethod
    def prepare_pending_command():
        arm_command = MagicMock(spec=Arm)
        arm_command.id = mavlink.MAV_CMD_COMPONENT_ARM_DISARM
        command_values = {
            'command': arm_command,
            'args': ['arg1', 'arg2'],
            'kwargs': {'kwarg1': 1, 'kwarg2': 2},
        }
        pending_command = MagicMock(spec=PendingCommand)
        pending_command.command = command_values['command']
        pending_command.args = command_values['args']
        pending_command.kwargs = command_values['kwargs']

        return pending_command

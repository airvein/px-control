"""test_ack_processor.py

Unit tests for AckProcessor class.

Copyright 2020 Cervi Robotics
"""
import logging
import unittest
from unittest.mock import MagicMock, patch

from pymavlink.mavutil import mavlink

from px_control.mavlink_controller.ack_processor import AckProcessor, AckStatus
from px_control.mavlink_controller.commands import *
from px_control.mavlink_controller.commands_manager import PendingCommand
from px_control.telemetry import Telemetry, Ack


class TestAckProcessor(unittest.TestCase):
    def setUp(self) -> None:
        self.telemetry = MagicMock(spec=Telemetry)
        self.pending_command = self.prepare_pending_command()
        patch_logger = patch('px_control.mavlink_controller.ack_processor.logger',
                             spec=logging.Logger)
        self.logger = patch_logger.start()
        self.addCleanup(patch_logger.stop)

        self.ack_processor = AckProcessor(telemetry=self.telemetry,
                                          pending_command=self.pending_command)

    def test_class_should_have_proper_class_vars_when_initialized(self):
        self.assertIsInstance(self.ack_processor._telemetry, Telemetry)
        self.assertIsInstance(self.ack_processor.pending_command, PendingCommand)

    def test_repeat_command_should_call_command_handler_with_proper_arguments(self):
        self.ack_processor._repeat_command()

        self.pending_command.command.handle.assert_called_once_with(
            *self.pending_command.args, **self.pending_command.kwargs
        )

    @patch.object(AckProcessor, '_check_ack')
    @patch.object(AckProcessor, '_ack_accepted')
    @patch.object(AckProcessor, '_ack_temporarily_rejected')
    @patch.object(AckProcessor, '_ack_denied')
    @patch.object(AckProcessor, '_ack_failed')
    @patch.object(AckProcessor, '_ack_unsupported')
    def test_process_should_call_proper_ack_handler_when_status_is_PROCESSING(
            self, _ack_unsupported, _ack_failed, _ack_denied,
            _ack_temporarily_rejected, _ack_accepted, _add_ack):
        self.ack_processor.status = AckStatus.PROCESSING
        mocked_ack_map = {
            None: _add_ack,
            mavlink.MAV_RESULT_ACCEPTED: _ack_accepted,
            mavlink.MAV_RESULT_TEMPORARILY_REJECTED: _ack_temporarily_rejected,
            mavlink.MAV_RESULT_DENIED: _ack_denied,
            mavlink.MAV_RESULT_FAILED: _ack_failed,
            mavlink.MAV_RESULT_UNSUPPORTED: _ack_unsupported,
        }
        self.ack_processor._ack_map = mocked_ack_map

        for result, method in mocked_ack_map.items():
            with self.subTest(result):
                self.ack_processor.result = result
                self.ack_processor.process()

                mocked_ack_map[result].assert_called_once()

    @patch.object(AckProcessor, '_get_result')
    def test_check_ack_should_cal_get_result_and_decrement_counter_when_counter_is_greater_than_zero(
            self, mock_get_result):
        self.ack_processor.check_counter = 3

        self.ack_processor._check_ack()

        mock_get_result.assert_called_once()
        self.assertEqual(2, self.ack_processor.check_counter)

    def test_check_ack_should_set_status_FAILED_and_log_warning_when_counter_is_not_greater_than_zero(
            self):
        self.ack_processor.check_counter = 0
        self.ack_processor._check_ack()

        self.assertEqual(AckStatus.FAILED, self.ack_processor.status)
        self.logger.warning.assert_called_once_with(
            f'NOT RECEIVED ACK FOR COMMAND {self.ack_processor.pending_command.command.id}'
        )

    def test_get_result_should_set_command_result_and_update_commands_ack(self):
        results = [result for result in self.ack_processor._ack_map.keys()]

        for result in results:
            with self.subTest(f'result = {result}'):
                self.telemetry.commands_ack = [
                    Ack(command=self.pending_command.command.id,
                        result=result),
                    Ack(command=self.pending_command.command.id,
                        result=result),
                    Ack(command=2137, result=11)
                ]

                self.ack_processor._get_result()

                self.assertEqual(result, self.ack_processor.result)
                self.assertEqual(1, len(self.telemetry.commands_ack))

    def test_ack_accepted_should_set_status_SUCCESS(self):
        self.ack_processor._ack_accepted()

        self.assertEqual(AckStatus.SUCCESS, self.ack_processor.status)

    @patch.object(AckProcessor, '_repeat_command')
    def test_ack_temporarily_rejected_should_repeat_command_and_set_result_to_None_when_counter_is_greater_than_zero(
            self, mock_repeat_command):
        self.ack_processor.repeat_counter = 2
        self.ack_processor.result = mavlink.MAV_RESULT_TEMPORARILY_REJECTED

        self.ack_processor._ack_temporarily_rejected()

        self.assertIsNone(self.ack_processor.result)
        mock_repeat_command.assert_called_once()
        self.assertEqual(1, self.ack_processor.repeat_counter)

    def test_ack_temporarily_rejected_should_set_status_FAILED_when_counter_is_not_greater_than_zero(
            self):
        self.ack_processor.repeat_counter = 0

        self.ack_processor._ack_temporarily_rejected()

        self.assertEqual(AckStatus.FAILED, self.ack_processor.status)

    def test_ack_denied_should_set_status_FAILED(self):
        self.ack_processor._ack_denied()

        self.assertEqual(AckStatus.FAILED, self.ack_processor.status)

    def test_ack_failed_should_set_status_FAILED(self):
        self.ack_processor._ack_failed()

        self.assertEqual(AckStatus.FAILED, self.ack_processor.status)

    def test_ack_unsupported_should_set_status_FAILED(self):
        self.ack_processor._ack_unsupported()

        self.assertEqual(AckStatus.FAILED, self.ack_processor.status)

    @staticmethod
    def prepare_pending_command():
        arm_command = MagicMock(spec=Arm)
        arm_command.id = mavlink.MAV_CMD_COMPONENT_ARM_DISARM
        command_values = {
            'command': arm_command,
            'args': ['arg1', 'arg2'],
            'kwargs': {'kwarg1': 1, 'kwarg2': 2},
        }
        pending_command = MagicMock(spec=PendingCommand)
        pending_command.command = command_values['command']
        pending_command.args = command_values['args']
        pending_command.kwargs = command_values['kwargs']

        return pending_command

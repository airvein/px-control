"""test_takeoff.py

Unit tests for takeoff

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import patch

from dronekit import Command
from pymavlink import mavutil

from px_control.mavlink_controller.mission.commands.takeoff import Takeoff


class TestTakeoff(unittest.TestCase):
    def test_takeoff_should_inherit_from_command(self):
        takeoff = Takeoff(altitude=15.0)
        self.assertIsInstance(takeoff, Command)

    @patch('px_control.mavlink_controller.mission.commands.takeoff.Command.__init__')
    def test_takeoff_should_call_command_init_with_correct_arguments(
            self, mock_command_init):
        takeoff_altitude = 15.0

        Takeoff(altitude=takeoff_altitude)

        mock_command_init.assert_called_once_with(
            0, 0, 0,
            mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
            mavutil.mavlink.MAV_CMD_NAV_TAKEOFF,
            0, 0, 0, 0, 0, 0, 0, 0,
            takeoff_altitude
        )

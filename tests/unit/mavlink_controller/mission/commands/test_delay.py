"""test_delay.py

Unit tests for delay

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import patch

from dronekit import Command
from pymavlink import mavutil

from px_control.mavlink_controller.mission.commands.delay import Delay


class TestDelay(unittest.TestCase):
    def test_delay_should_inherit_from_command(self):
        delay = Delay(time=2)
        self.assertIsInstance(delay, Command)

    @patch('px_control.mavlink_controller.mission.commands.delay.Command.__init__')
    def test_delay_should_call_command_init_with_correct_arguments(
            self, mock_command_init):
        time = 4

        Delay(time=time)

        mock_command_init.assert_called_once_with(
            0, 0, 0,
            mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
            mavutil.mavlink.MAV_CMD_NAV_DELAY,
            0, 0, time, -1, -1, -1, 0, 0, 0
        )

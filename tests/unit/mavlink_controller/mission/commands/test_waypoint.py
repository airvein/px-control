"""test_waypoint.py

Unit tests for waypoint

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import patch

from dronekit import Command
from pymavlink import mavutil

from px_control.mavlink_controller.mission.commands.waypoint import Waypoint


class TestWaypoint(unittest.TestCase):
    def test_waypoint_should_inherit_from_command(self):
        waypoint = Waypoint(latitude=1.1, longitude=2.2, altitude=3.3)
        self.assertIsInstance(waypoint, Command)

    @patch('px_control.mavlink_controller.mission.commands.waypoint.Command.__init__')
    def test_waypoint_should_call_command_init_with_correct_arguments(
            self, mock_command_init):
        waypoint_latitude = 1.1
        waypoint_longitude = 2.2
        waypoint_altitude = 3.3

        Waypoint(latitude=waypoint_latitude,
                 longitude=waypoint_longitude,
                 altitude=waypoint_altitude)

        mock_command_init.assert_called_once_with(
            0, 0, 0,
            mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
            mavutil.mavlink.MAV_CMD_NAV_WAYPOINT,
            0, 0, 0, 0, 0, 0,
            waypoint_latitude, waypoint_longitude, waypoint_altitude
        )

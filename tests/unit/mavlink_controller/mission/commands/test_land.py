"""test_land.py

Unit tests for land

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import patch

from dronekit import Command
from pymavlink import mavutil

from px_control.mavlink_controller.mission.commands.land import Land


class TestLand(unittest.TestCase):
    def test_land_should_inherit_from_command(self):
        land = Land(latitude=1.1, longitude=2.2)
        self.assertIsInstance(land, Command)

    @patch('px_control.mavlink_controller.mission.commands.land.Command.__init__')
    def test_land_should_call_command_init_with_correct_arguments(
            self, mock_command_init):
        land_latitude = 1.1
        land_longitude = 2.2

        Land(latitude=land_latitude, longitude=land_longitude)

        mock_command_init.assert_called_once_with(
            0, 0, 0,
            mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
            mavutil.mavlink.MAV_CMD_NAV_LAND,
            0, 0, 0, mavutil.mavlink.PRECISION_LAND_MODE_REQUIRED,
            0, 0, land_latitude, land_longitude, 0
        )

"""test_change_yaw.py

Unit tests for change_yaw

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import patch

from dronekit import Command
from pymavlink import mavutil

from px_control.mavlink_controller.mission.commands.change_yaw import ChangeYaw


class TestChangeYaw(unittest.TestCase):
    def test_change_yaw_should_inherit_from_command(self):
        change_yaw = ChangeYaw(degree=270)
        self.assertIsInstance(change_yaw, Command)

    @patch('px_control.mavlink_controller.mission.commands.change_yaw.Command.__init__')
    def test_change_yaw_should_call_command_init_with_correct_arguments(
            self, mock_command_init):
        yaw_degree = 270

        ChangeYaw(degree=yaw_degree)

        mock_command_init.assert_called_once_with(
            0, 0, 0,
            mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
            mavutil.mavlink.MAV_CMD_CONDITION_YAW,
            0, 0, yaw_degree, 0, 0, 0, 0, 0, 0
        )

"""test_change_ground_speed.py

Unit tests for change_ground_speed

Copyright 2020 Cervi Robotics
"""

import unittest
from unittest.mock import patch

from dronekit import Command
from pymavlink import mavutil

from px_control.mavlink_controller.mission.commands.change_ground_speed import \
    ChangeGroundSpeed


class TestChangeGroundSpeed(unittest.TestCase):
    def test_change_ground_speed_should_inherit_from_command(self):
        change_ground_speed = ChangeGroundSpeed(ground_speed=1.1)
        self.assertIsInstance(change_ground_speed, Command)

    @patch('px_control.mavlink_controller.mission.commands.change_ground_speed.Command.__init__')
    def test_change_ground_speed_should_call_command_init_with_correct_arguments(
            self, mock_command_init):
        ground_speed = 1.1
        ChangeGroundSpeed(ground_speed=ground_speed)

        mock_command_init.assert_called_once_with(
            0, 0, 0,
            mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
            mavutil.mavlink.MAV_CMD_DO_CHANGE_SPEED,
            0, 0, 1, ground_speed, 0, 0, 0, 0, 0
        )

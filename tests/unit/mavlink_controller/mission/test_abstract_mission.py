"""test_abstract_mission.py

Unit tests for abstract_mission

Copyright 2020 Cervi Robotics
"""
from abc import ABC
import json
import unittest
from threading import Thread
from unittest.mock import MagicMock, patch

import dronekit

from px_control.mavlink_controller.mission.commands.takeoff import Takeoff
from px_control.mavlink_controller.mission.commands.land import Land
from px_control.mavlink_controller.mission.commands.waypoint import Waypoint
from px_control.mavlink_controller.mission.abstract_mission import \
    AbstractMission
from tests.unit.fixtures.normal_route import normal_route_fixture


class TestAbstractMission(unittest.TestCase):

    def setUp(self) -> None:
        self.drone = MagicMock(spec=dronekit.Vehicle)
        self.route = normal_route_fixture

        self.abstract_mission = AbstractMission(drone=self.drone,
                                                route=self.route)

    def test_abstract_mission_should_be_instance_of_ABC(self):
        self.assertIsInstance(self.abstract_mission, ABC)

    def test_abstract_mission_should_create_proper_variables_when_initialized(
            self):
        self.assertIsInstance(self.abstract_mission._drone, dronekit.Vehicle)
        self.assertIsInstance(self.abstract_mission._route, dict)
        self.assertIsInstance(self.abstract_mission.mission_commands, list)
        self.assertFalse(self.abstract_mission._uploading)

    def test_abstract_mission_should_initialize_proper_variables_as_Nones(
            self):
        self.assertIsNone(self.abstract_mission.takeoff_index)
        self.assertIsNone(self.abstract_mission.landing_index)
        self.assertIsNone(self.abstract_mission._points)
        self.assertIsNone(self.abstract_mission._zones)
        self.assertIsNone(self.abstract_mission._landing_yaw)

    @patch.object(AbstractMission, '_clear_drone_mission')
    def test_prepare_should_clear_drone_mission_and_prepare_drone_for_uploading_commands(
            self, mock__clear_drone_mission):
        self.drone.commands = MagicMock(spec=dronekit.CommandSequence)

        self.abstract_mission.prepare()

        mock__clear_drone_mission.assert_called_once()
        self.drone.commands.download.assert_called_once()
        self.drone.commands.wait_ready.assert_called_once()

    @patch('px_control.mavlink_controller.mission.abstract_mission.threading.Thread')
    def test_upload_should_set_uploading_flag_true_and_create_mission_thread(
            self, mock_threading):
        mock_thread = MagicMock(spec=Thread)
        mock_threading.return_value = mock_thread

        self.abstract_mission.upload()

        self.assertTrue(self.abstract_mission._uploading)

        mock_threading.assert_called_once_with(
            target=self.abstract_mission._upload_mission, daemon=True)
        mock_thread.start.assert_called_once()

    def test_upload_mission_should_upload_mission_commands_set_next_command_to_zero_and_set_uploading_false(
            self):
        self.drone.commands = MagicMock(spec=dronekit.CommandSequence)
        self.abstract_mission.mission_commands.append(
            MagicMock(spec=dronekit.Command))

        self.abstract_mission._upload_mission()

        self.drone.commands.add.assert_called_once()
        self.drone.commands.upload.assert_called_once()
        self.assertEqual(0, self.drone.commands.next)
        self.assertFalse(self.abstract_mission._uploading)

    def test_clear_drone_mission_should_clear_mission(self):
        self.drone.commands = MagicMock(spec=dronekit.CommandSequence)

        self.abstract_mission._clear_drone_mission()

        self.drone.commands.clear.assert_called_once()
        self.drone.commands.upload.assert_called_once()

    def test_parse_points_should_get_points_from_route(self):
        expected_points = json.loads(normal_route_fixture)['route_points']

        self.abstract_mission._parse_points()

        self.assertIsInstance(self.abstract_mission._points, list)
        self.assertEqual(expected_points, self.abstract_mission._points)

    def test_parse_zones_should_get_zones_from_route(self):
        expected_zones = json.loads(normal_route_fixture)['zones']

        self.abstract_mission._parse_zones()

        self.assertIsInstance(self.abstract_mission._zones, dict)
        self.assertEqual(expected_zones, self.abstract_mission._zones)

    def test_parse_landing_yaw_should_get_yaw_from_route(self):
        landing_yaw = json.loads(normal_route_fixture)['landing_yaw']

        self.abstract_mission._parse_landing_yaw()

        self.assertIsInstance(self.abstract_mission._landing_yaw, float)
        self.assertEqual(landing_yaw, self.abstract_mission._landing_yaw)

    def test_find_takeoff_index_should_set_takeoff_index(self):
        self._prepare_mission_commands()

        self.abstract_mission._find_takeoff_index()

        self.assertEqual(1, self.abstract_mission.takeoff_index)

    def test_find_landing_index_should_set_takeoff_index(self):
        self._prepare_mission_commands()

        self.abstract_mission._find_landing_index()

        self.assertEqual(len(self.abstract_mission.mission_commands),
                         self.abstract_mission.landing_index)

    def _prepare_mission_commands(self):
        self.abstract_mission.mission_commands.append(MagicMock(spec=Takeoff))
        for i in range(10):
            self.abstract_mission.mission_commands.append(
                MagicMock(spec=Waypoint)
            )
        self.abstract_mission.mission_commands.append(MagicMock(spec=Land))

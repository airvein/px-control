"""test_emergency_mission.py

Unit tests for emergency_mission

Copyright 2020 Cervi Robotics
"""
import json
import unittest
from unittest.mock import MagicMock, patch

import dronekit

from px_control.mavlink_controller.mission.abstract_mission import \
    AbstractMission
from px_control.mavlink_controller.mission.emergency_mission import \
    EmergencyMission
from px_control.mavlink_controller.mission.commands.waypoint import Waypoint
from px_control.mavlink_controller.mission.commands.land import Land
from px_control.mavlink_controller.mission.commands.change_ground_speed import \
    ChangeGroundSpeed
from tests.unit.fixtures.emergency_route import emergency_route_fixture
from px_control import settings


class TestEmergencyMission(unittest.TestCase):
    def setUp(self) -> None:
        self.mock_drone = MagicMock(spec=dronekit.Vehicle)
        self.route = emergency_route_fixture
        self.emergency_mission = EmergencyMission(drone=self.mock_drone,
                                                  route=self.route)

    def test_emergency_mission_should_inherit_from_abstract_mission(self):
        self.assertIsInstance(self.emergency_mission, AbstractMission)

    @patch.object(EmergencyMission, '_parse_zones')
    @patch.object(EmergencyMission, '_parse_points')
    def test_emergency_mission_should_parse_points_and_zones_when_initialized(
            self, mock_parse_points, mock_parse_zones):
        EmergencyMission(drone=self.mock_drone,
                         route=self.route)

        mock_parse_points.assert_called_once()
        mock_parse_zones.assert_called_once()

    @patch.object(AbstractMission, 'prepare')
    def test_prepare_should_call_prepare_from_abstract_mission_class(
            self, mock_abstract_prepare):
        self.emergency_mission.prepare()

        mock_abstract_prepare.assert_called_once()

    def test_prepare_should_add_change_ground_speed_as_first_command_to_mission_commands(
            self):
        speed_command = ChangeGroundSpeed(settings.Mission.CRUISE_SPEED)

        self.emergency_mission.prepare()

        self.assertIn(speed_command, self.emergency_mission.mission_commands)
        self.assertEqual(
            0, self.emergency_mission.mission_commands.index(speed_command)
        )

    def test_prepare_should_add_land_as_last_command_to_mission_commands(
            self):
        land_command = Land(latitude=self.emergency_mission._points[-1][1],
                            longitude=self.emergency_mission._points[-1][2])

        self.emergency_mission.prepare()

        self.assertIn(land_command, self.emergency_mission.mission_commands)
        self.assertEqual(
            len(self.emergency_mission.mission_commands) - 1,
            self.emergency_mission.mission_commands.index(land_command)
        )

    def test_prepare_should_add_waypoints_to_mission_commands(self):
        route_points = json.loads(self.route)['route_points']

        self.emergency_mission.prepare()

        for point in route_points:
            with self.subTest(msg=point[0]):
                expected_waypoint = Waypoint(latitude=point[1],
                                             longitude=point[2],
                                             altitude=point[3])
                self.assertIn(
                    expected_waypoint, self.emergency_mission.mission_commands
                )

    @patch.object(EmergencyMission, '_find_landing_index')
    def test_prepare_should_call_find_landing_index(
            self, mock_find_landing_index):
        self.emergency_mission.prepare()

        mock_find_landing_index.assert_called_once()

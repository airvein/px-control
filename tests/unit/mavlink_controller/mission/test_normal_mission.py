"""test_normal_mission.py

Unit tests for normal_mission

Copyright 2020 Cervi Robotics
"""
import json
import unittest
from unittest.mock import MagicMock, patch

import dronekit

from px_control.mavlink_controller.mission.abstract_mission import \
    AbstractMission
from px_control.mavlink_controller.mission.commands.delay import Delay
from px_control.mavlink_controller.mission.normal_mission import \
    NormalMission
from px_control.mavlink_controller.mission.commands.waypoint import Waypoint
from px_control.mavlink_controller.mission.commands.change_ground_speed import \
    ChangeGroundSpeed
from tests.unit.fixtures.normal_route import normal_route_fixture
from px_control import settings


class TestNormalMission(unittest.TestCase):
    def setUp(self) -> None:
        self.mock_drone = MagicMock(spec=dronekit.Vehicle)
        self.route = normal_route_fixture
        self.normal_mission = NormalMission(drone=self.mock_drone,
                                            route=self.route)

    def test_normal_mission_should_inherit_from_abstract_mission(self):
        self.assertIsInstance(self.normal_mission, AbstractMission)

    @patch.object(NormalMission, '_parse_landing_yaw')
    @patch.object(NormalMission, '_parse_zones')
    @patch.object(NormalMission, '_parse_points')
    def test_normal_mission_should_parse_points_and_zones_when_initialized(
            self, mock_parse_points, mock_parse_zones, mock_parse_landing_yaw):
        NormalMission(drone=self.mock_drone,
                      route=self.route)

        mock_parse_points.assert_called_once()
        mock_parse_zones.assert_called_once()
        mock_parse_landing_yaw.assert_called_once()

    @patch.object(AbstractMission, 'prepare')
    def test_prepare_should_call_prepare_from_abstract_mission_class(
            self, mock_abstract_prepare):
        self.normal_mission.prepare()

        mock_abstract_prepare.assert_called_once()

    @patch('px_control.mavlink_controller.mission.normal_mission.delay.Delay')
    def test_prepare_should_add_delay_as_first_command_to_mission_commands(
            self, mock_delay):
        expected_delay = mock_delay()

        self.normal_mission.prepare()

        self.assertIn(expected_delay, self.normal_mission.mission_commands)
        self.assertEqual(
            0, self.normal_mission.mission_commands.index(expected_delay)
        )

    @patch('px_control.mavlink_controller.mission.normal_mission.takeoff.Takeoff')
    def test_prepare_should_add_takeoff_as_second_command_to_mission_commands(
            self, mock_takeoff):
        expected_takeoff = mock_takeoff()

        with patch.object(AbstractMission, '_find_takeoff_index'):
            self.normal_mission.prepare()

            self.assertIn(expected_takeoff, self.normal_mission.mission_commands)
            self.assertEqual(
                1, self.normal_mission.mission_commands.index(expected_takeoff)
            )

    @patch('px_control.mavlink_controller.mission.normal_mission.land.Land')
    def test_prepare_should_add_land_as_last_command_to_mission_commands(
            self, mock_land):
        expected_land = mock_land()

        with patch.object(AbstractMission, '_find_landing_index'):
            self.normal_mission.prepare()

            self.assertIn(expected_land, self.normal_mission.mission_commands)
            self.assertEqual(
                len(self.normal_mission.mission_commands) - 1,
                self.normal_mission.mission_commands.index(expected_land)
            )

    @patch('px_control.mavlink_controller.mission.normal_mission.change_yaw.ChangeYaw')
    def test_prepare_should_add_change_yaw_command_before_landing_delay(
            self, mock_change_yaw):
        expected_change_yaw = mock_change_yaw()
        mock_change_yaw.reset_mock()

        self.normal_mission.prepare()
        self.assertIn(expected_change_yaw, self.normal_mission.mission_commands)
        self.assertEqual(
            len(self.normal_mission.mission_commands) - 3,
            self.normal_mission.mission_commands.index(expected_change_yaw)
        )
        mock_change_yaw.assert_called_once_with(
            self.normal_mission._landing_yaw
        )

    @patch('px_control.mavlink_controller.mission.normal_mission.change_yaw.ChangeYaw')
    def test_prepare_should_add_delay_command_before_and_after_change_yaw(
            self, mock_change_yaw):
        mission_commands = self.normal_mission.mission_commands
        expected_change_yaw = mock_change_yaw()

        self.normal_mission.prepare()

        change_yaw_index = mission_commands.index(expected_change_yaw)
        self.assertIn(expected_change_yaw, self.normal_mission.mission_commands)
        self.assertIsInstance(mission_commands[change_yaw_index - 1], Delay)
        self.assertIsInstance(mission_commands[change_yaw_index + 1], Delay)

    def test_prepare_should_add_waypoints_to_mission_commands(self):
        route_points = json.loads(self.route)['route_points']

        self.normal_mission.prepare()

        for point in route_points:
            with self.subTest(msg=point[0]):
                expected_waypoint = Waypoint(latitude=point[1],
                                             longitude=point[2],
                                             altitude=point[3])
                self.assertIn(
                    expected_waypoint, self.normal_mission.mission_commands
                )

    @patch.object(NormalMission, '_adjust_zone_speed')
    def test_prepare_should_call_adjust_zone_speed_when_adding_every_waypoint(
            self, mock_adjust_zone_speed):
        expected_call_count = len(json.loads(self.route)['route_points'])

        self.normal_mission.prepare()

        self.assertEqual(expected_call_count, mock_adjust_zone_speed.call_count)

    def test_adjust_zone_speed_should_add_speed_commands_before_proper_points(
            self):
        route_zones = json.loads(self.route)['zones']

        cases = [
            {'point_id': route_zones['home_maneuver'],
             'speed': settings.Mission.MANEUVER_SPEED},

            {'point_id': route_zones['landing_maneuver'] + 1,
             'speed': settings.Mission.MANEUVER_SPEED},

            {'point_id': route_zones['home_approach'],
             'speed': settings.Mission.APPROACH_SPEED},

            {'point_id': route_zones['landing_approach'] + 1,
             'speed': settings.Mission.APPROACH_SPEED},

            {'point_id': route_zones['home_approach'] + 1,
             'speed': settings.Mission.CRUISE_SPEED},

            {'point_id': route_zones['landing_approach'] - 1,
             'speed': settings.Mission.BEFORE_APPROACH_SPEED[0]},

            {'point_id': route_zones['landing_approach'],
             'speed': settings.Mission.BEFORE_APPROACH_SPEED[1]},
        ]

        for case in cases:
            with self.subTest(name=case['point_id']):
                self.normal_mission._adjust_zone_speed(case['point_id'])

                self.assertIn(ChangeGroundSpeed(ground_speed=case['speed']),
                              self.normal_mission.mission_commands)

    @patch.object(NormalMission, '_find_landing_index')
    @patch.object(NormalMission, '_find_takeoff_index')
    def test_prepare_should_call_find_landing_index_and_find_takeoff_index(
            self, mock_find_landing_index, mock_find_takeoff_index):
        self.normal_mission.prepare()

        mock_find_takeoff_index.assert_called()
        mock_find_landing_index.assert_called()

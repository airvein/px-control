"""test_upload_mission.py

Unit tests for mission

Copyright 2020 Cervi Robotics
"""
import unittest
from unittest.mock import MagicMock, patch

import dronekit

from px_control.common.constants import MissionTypes
from px_control.mavlink_controller.commands.abstract_command import \
    AbstractCommand
from px_control.mavlink_controller.commands import UploadMission
from px_control.telemetry import Telemetry


class TestUploadMission(unittest.TestCase):
    @patch('px_control.mavlink_controller.commands.upload_mission.NormalMission')
    @patch('px_control.mavlink_controller.commands.upload_mission.EmergencyMission')
    @patch('px_control.mavlink_controller.commands.upload_mission.RthMission')
    def setUp(self, rth_mission, emergency_mission, normal_mission) -> None:
        self.drone = MagicMock(spec=dronekit.Vehicle)
        self.telemetry = MagicMock(spec=Telemetry)

        self.mission_mock_map = {
            MissionTypes.NORMAL: normal_mission,
            MissionTypes.EMERGENCY: emergency_mission,
            MissionTypes.RTH: rth_mission,
        }

        self.mission = UploadMission(drone=self.drone, telemetry=self.telemetry)

    def test_handle_should_prepare_and_upload_correct_mission(self):
        expected_route = 'some_data'

        for mission_type, mock in self.mission_mock_map.items():
            with self.subTest(msg=f'Mission type: {mission_type}'):
                self.mission.handle(mission_type, expected_route)

                mock.assert_called_once_with(
                    drone=self.drone, route=expected_route
                )
                self.telemetry.current_mission.prepare.assert_called_once()
                self.telemetry.current_mission.upload.assert_called_once()

    def test_mission_should_inherit_from_abstract_command(self):
        self.assertIsInstance(self.mission, AbstractCommand)

"""test_abstract_command.py

Unit tests for abstract_command

Copyright 2020 Cervi Robotics
"""
import unittest
from abc import ABC
from unittest.mock import MagicMock

import dronekit

from px_control.mavlink_controller.commands.abstract_command import \
    AbstractCommand


class TestAbstractCommand(unittest.TestCase):
    def setUp(self) -> None:
        self.drone = MagicMock(spec=dronekit.Vehicle)

        self.abstract_command = AbstractCommand(drone=self.drone)

    def test_abstract_command_should_inherit_from_ABC(self):
        self.assertIsInstance(self.abstract_command, ABC)

    def test_abstract_command_should_have_None_id(self):
        self.assertIsNone(self.abstract_command.id)

    def test_abstract_command_should_have_drone_and_logger_when_initialized(self):
        self.assertIsInstance(self.abstract_command._drone, dronekit.Vehicle)

    def test_handle_should_raise_not_implemented_error(self):
        with self.assertRaises(NotImplementedError):
            self.abstract_command.handle()

"""test_arm.py

Unit tests for arm

Copyright 2020 Cervi Robotics
"""
import logging
import unittest
from unittest.mock import MagicMock, patch

import dronekit
from pymavlink import mavutil

from px_control.mavlink_controller.commands.abstract_command import \
    AbstractCommand
from px_control.mavlink_controller.commands.arm import Arm


class TestArm(unittest.TestCase):
    def setUp(self) -> None:
        self.drone = MagicMock(spec=dronekit.Vehicle)
        patch_logger = patch('px_control.mavlink_controller.commands.arm.logger',
                             spec=logging.Logger)
        self.logger = patch_logger.start()
        self.addCleanup(patch_logger.stop)

        self.arm = Arm(drone=self.drone)

    def test_arm_should_inherit_from_abstract_command(self):
        self.assertIsInstance(self.arm, AbstractCommand)

    def test_arm_should_have_id_equal_to_arm_message(self):
        self.assertEqual(mavutil.mavlink.MAV_CMD_COMPONENT_ARM_DISARM,
                         self.arm.id)

    def test_handle_should_call_drone_arm(self):
        self.arm.handle()

        self.drone.arm.assert_called_with(timeout=2)

    def test_handle_should_log_error_when_got_timeout_error(self):
        self.drone.arm.side_effect = dronekit.TimeoutError
        self.arm.handle()

        self.logger.error.assert_called_with("Failed to arm drone")

"""test_set_mode.py

Unit tests for set_mode

Copyright 2020 Cervi Robotics
"""
import logging
import unittest
from unittest.mock import MagicMock, patch

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common import constants
from px_control.mavlink_controller.commands.abstract_command import \
    AbstractCommand
from px_control.mavlink_controller.commands.set_mode import SetMode


class TestSetMode(unittest.TestCase):
    def setUp(self) -> None:
        self.drone = MagicMock(spec=dronekit.Vehicle)
        patch_logger = patch(
            'px_control.mavlink_controller.commands.set_mode.logger',
            spec=logging.Logger
        )
        self.logger = patch_logger.start()
        self.addCleanup(patch_logger.stop)

        self.set_mode = SetMode(drone=self.drone)

    def test_set_mode_should_inherit_from_abstract_command(self):
        self.assertIsInstance(self.set_mode, AbstractCommand)

    @patch('px_control.mavlink_controller.commands.set_mode.settings.Mission')
    def test_set_mode_should_have_id_equal_to_set_mode_message(
            self, mock_mission):
        cases = [
            {'deprecated': False, 'command_id': pm.MAV_CMD_DO_SET_MODE},
            {'deprecated': True, 'command_id': pm.MAVLINK_MSG_ID_SET_MODE},
        ]
        for case in cases:
            mock_mission.USE_DEPRECATED_SET_MODE_COMMAND = case['deprecated']

            set_mode = SetMode(drone=self.drone)

            self.assertEqual(case['command_id'], set_mode.id)

    def test_handle_should_call_drone_wait_for_mode(self):
        expected_mode = constants.AutopilotModes.AUTO

        self.set_mode.handle(expected_mode)

        self.drone.wait_for_mode.assert_called_with(expected_mode, timeout=2.0)

    def test_handle_should_log_error_when_got_timeout_error(self):
        expected_mode = constants.AutopilotModes.AUTO
        self.drone.wait_for_mode.side_effect = dronekit.TimeoutError

        self.set_mode.handle(expected_mode)

        self.logger.error.assert_called_with(f"Failed to set: {expected_mode} mode")

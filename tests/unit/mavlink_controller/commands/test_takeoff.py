"""test_takeoff.py

Unit tests for takeoff

Copyright 2020 Cervi Robotics
"""
import unittest
from unittest.mock import MagicMock

import dronekit
from pymavlink import mavutil

from px_control.mavlink_controller.commands.abstract_command import \
    AbstractCommand
from px_control.mavlink_controller.commands.takeoff import Takeoff


class TestTakeoff(unittest.TestCase):
    def setUp(self) -> None:
        self.drone = MagicMock(spec=dronekit.Vehicle)

        self.takeoff = Takeoff(drone=self.drone)

    def test_takeoff_should_inherit_from_abstract_command(self):
        self.assertIsInstance(self.takeoff, AbstractCommand)

    def test_takeoff_should_have_id_equal_to_nav_takeoff_message(self):
        self.assertEqual(mavutil.mavlink.MAV_CMD_NAV_TAKEOFF,
                         self.takeoff.id)

    def test_handle_should_call_drone_takeoff(self):
        expected_altitude = 21.37

        self.takeoff.handle(expected_altitude)
        self.drone.wait_simple_takeoff.assert_called_with(
            alt=expected_altitude, epsilon=0.5
        )

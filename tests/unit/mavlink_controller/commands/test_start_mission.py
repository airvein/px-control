"""test_start_mission.py

Unit tests for start_mission

Copyright 2020 Cervi Robotics
"""
import unittest
from unittest.mock import MagicMock

import dronekit
from pymavlink import mavutil

from px_control.mavlink_controller.commands.abstract_command import \
    AbstractCommand
from px_control.mavlink_controller.commands.start_mission import \
    StartMission


class TestStartMission(unittest.TestCase):
    def setUp(self) -> None:
        self.drone = MagicMock(spec=dronekit.Vehicle)

        self.start_mission = StartMission(drone=self.drone)

    def test_start_mission_should_inherit_from_abstract_command(self):
        self.assertIsInstance(self.start_mission, AbstractCommand)

    def test_start_mission_should_have_id_equal_to_mission_start_message(self):
        self.assertEqual(mavutil.mavlink.MAV_CMD_MISSION_START,
                         self.start_mission.id)

    def test_handle_should_prepare_proper_message(self):
        self.start_mission.handle()

        self.drone.message_factory.command_long_encode.assert_called_with(
            target_system=0, target_component=0,
            command=mavutil.mavlink.MAV_CMD_MISSION_START,
            confirmation=0,
            param1=0, param2=1, param3=0, param4=0,
            param5=0, param6=0, param7=0
        )

    def test_handle_should_send_proper_message(self):
        message = "test_message"
        self.drone.message_factory.command_long_encode.return_value = message

        self.start_mission.handle()

        self.drone.send_mavlink.assert_called_with(message)
        self.drone.commands.upload.assert_called_once()

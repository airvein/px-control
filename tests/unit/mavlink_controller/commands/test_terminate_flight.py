"""test_terminate_flight.py

Unit tests for terminate_flight

Copyright 2020 Cervi Robotics
"""
import logging
import unittest
from unittest.mock import MagicMock, patch

import dronekit
from pymavlink import mavutil

from px_control.mavlink_controller.commands.abstract_command import \
    AbstractCommand
from px_control.mavlink_controller.commands.terminate_flight import \
    TerminateFlight


class TerTerminateFlight(unittest.TestCase):
    def setUp(self) -> None:
        self.drone = MagicMock(spec=dronekit.Vehicle)
        patch_logger = patch(
            'px_control.mavlink_controller.commands.terminate_flight.logger',
            spec=logging.Logger
        )
        self.logger = patch_logger.start()
        self.addCleanup(patch_logger.stop)
        self.terminate_flight = TerminateFlight(drone=self.drone)

    def test_terminate_flight_inherit_from_abstract_command(self):
        self.assertIsInstance(self.terminate_flight, AbstractCommand)

    def test_terminate_flight_should_have_id_equal_to_do_flighttermination_message(self):
        self.assertEqual(mavutil.mavlink.MAV_CMD_DO_FLIGHTTERMINATION,
                         self.terminate_flight.id)

    def test_handle_should_prepare_proper_message(self):
        self.terminate_flight.handle()

        self.logger.warning.assert_called_with("TERMINATING FLIGHT")
        self.drone.message_factory.command_long_encode.assert_called_with(
            target_system=0, target_component=0,
            command=mavutil.mavlink.MAV_CMD_DO_FLIGHTTERMINATION,
            confirmation=0,
            param1=1, param2=0, param3=0, param4=0,
            param5=0, param6=0, param7=0
        )

    def test_handle_should_send_proper_message(self):
        message = "test_message"
        self.drone.message_factory.command_long_encode.return_value = message

        self.terminate_flight.handle()

        self.drone.send_mavlink.assert_called_with(message)
        self.drone.commands.upload.assert_called_once()

"""test_set_groundspeed.py

Unit tests for set_groundspeed

Copyright 2020 Cervi Robotics
"""
import unittest
from unittest.mock import MagicMock

import dronekit
from pymavlink import mavutil

from px_control.mavlink_controller.commands.abstract_command import \
    AbstractCommand
from px_control.mavlink_controller.commands.set_groundspeed import \
    SetGroundspeed


class TestSetGroundspeed(unittest.TestCase):
    def setUp(self) -> None:
        self.drone = MagicMock(spec=dronekit.Vehicle)

        self.set_groundspeed = SetGroundspeed(drone=self.drone)

    def test_set_ground_speed_should_inherit_from_abstract_command(self):
        self.assertIsInstance(self.set_groundspeed, AbstractCommand)

    def test_set_ground_speed_should_have_id_equal_to_do_change_speed_message(self):
        self.assertEqual(mavutil.mavlink.MAV_CMD_DO_CHANGE_SPEED,
                         self.set_groundspeed.id)

    def test_handle_should_set_ground_speed_and_log_change(self):
        expected_velocity = 21.37

        self.set_groundspeed.handle(expected_velocity)

        self.assertEqual(expected_velocity, self.drone.groundspeed)

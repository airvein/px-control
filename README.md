# AirVein Pixhawk controller module

## Description

Module provides support for controlling Pixhawk-compatible controllers and 
publishes telemetry data.

### Requirements

- Installed ROS2 Bouncy
- [Drone types](https://git.servocode.com/AirVein/drone-types)

## Quick start

### Create environment

```
mkdir airvein_ws/src && cd airvein_ws/src
```

### Grab dependencies

```
git clone https://git.servocode.com/AirVein/drone-types
```

### Built project

Inside ```airvein_ws``` directory
```
colcon build
```


### Run tests

Inside ```airvein_ws``` directory
```
colcon test
```

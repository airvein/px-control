import os
from pymavlink.mavutil import mavlink


class Connection:
    TYPE = "UDP" if os.environ.get('PX_SITL') == '1' else "SERIAL"
    AUTOPILOT = "APM"
    DEVICE = "/dev/ttyS4"
    BAUD = 115200
    URL = "udp:127.0.0.1:14550"


class Mission:
    USE_DEPRECATED_SET_MODE_COMMAND = \
        os.environ.get('PX_USE_DEPRECATED_SET_MODE_COMMAND') == '1'

    TAKEOFF_ALTITUDE = 15
    TAKEOFF_DELAY = 4  # seconds
    CHANGE_YAW_DELAY = 2  # seconds
    LANDING_DELAY = 4

    CRUISE_SPEED = 5.0
    APPROACH_SPEED = 3.0
    MANEUVER_SPEED = 2.0
    BEFORE_APPROACH_SPEED = (4.5, 3.5)


class AckCounters:
    REPEAT_COMMAND = 5
    CHECK_ACK = 5
    REPEAT_MISSION = 5
    CHECK_MISSION = 5


class Timers:
    TELEMETRY_UPDATE_FREQ = 20  # Hz
    COMMANDS_EXECUTE_FREQ = 10  # Hz
    HEARTBEAT_UPDATE_FREQ = 1  # Hz
    CAMERA_CHECKER_UPDATE_FREQ = 1  # Hz


class Safety:
    VIBRATION_TOLERANCE = 30.0
    STATUS_TEXT_SEVERITY = (
        mavlink.MAV_SEVERITY_CRITICAL, mavlink.MAV_SEVERITY_ALERT,
        mavlink.MAV_SEVERITY_EMERGENCY, mavlink.MAV_SEVERITY_ERROR
    )

    class Camera:
        AVAILABLE = os.environ.get('PX_CAMERA_AVAILABLE') == '1'
        CAMERA_NAME = 'camera0'
        FEED_SERVICE = 'rtsp_feed'
        STREAM_SERVICE = 'rtsp_stream'

    class GpsTolerance:
        EPV = 200
        EPH = 200
        SAT_COUNT = 12

    class EkfVariance:
        VELOCITY = 0.2
        POS_HORIZ = 0.2
        POS_VERT = 0.2
        COMPASS = 0.2

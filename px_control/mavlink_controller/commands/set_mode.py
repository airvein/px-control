"""set_mode.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control import settings
from px_control.mavlink_controller.commands.abstract_command import \
    AbstractCommand

logger = logging.getLogger(__name__)


class SetMode(AbstractCommand):
    def __init__(self, drone: dronekit.Vehicle):
        super().__init__(drone)
        if settings.Mission.USE_DEPRECATED_SET_MODE_COMMAND:
            self.id = pm.MAVLINK_MSG_ID_SET_MODE
        else:
            self.id = pm.MAV_CMD_DO_SET_MODE

    def handle(self, mode: str):
        try:
            logger.info(f"Changing mode to: {mode}")
            self._drone.wait_for_mode(mode, timeout=2.0)

        except dronekit.TimeoutError:
            logger.error(f"Failed to set: {mode} mode")

"""set_groundspeed.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from pymavlink import mavutil

from px_control.mavlink_controller.commands.abstract_command import \
    AbstractCommand

logger = logging.getLogger(__name__)


class SetGroundspeed(AbstractCommand):
    def __init__(self, drone: dronekit.Vehicle):
        super().__init__(drone)
        self.id = mavutil.mavlink.MAV_CMD_DO_CHANGE_SPEED

    def handle(self, velocity: float):
        logger.debug(f"Changing groundspeed to: {velocity}")
        self._drone.groundspeed = velocity

"""abstract_command.py


Copyright 2020 Cervi Robotics
"""
from abc import ABC
from typing import Optional

import dronekit


class AbstractCommand(ABC):
    def __init__(self, drone: dronekit.Vehicle):
        self._drone = drone
        self.id: Optional[int] = None

    def handle(self, *args, **kwargs):
        raise NotImplementedError("Not implemented")

"""start_mission.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from pymavlink import mavutil

from px_control.mavlink_controller.commands.abstract_command import \
    AbstractCommand

logger = logging.getLogger(__name__)


class StartMission(AbstractCommand):
    def __init__(self, drone: dronekit.Vehicle):
        super().__init__(drone)
        self.id = mavutil.mavlink.MAV_CMD_MISSION_START

    def handle(self):
        msg = self._drone.message_factory.command_long_encode(
            target_system=0, target_component=0,
            command=self.id,
            confirmation=0,
            param1=0, param2=1, param3=0, param4=0,
            param5=0, param6=0, param7=0
        )
        logger.info("Starting mission...")
        self._drone.send_mavlink(msg)
        self._drone.commands.upload()

"""takeoff.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from pymavlink import mavutil

from px_control.mavlink_controller.commands.abstract_command import \
    AbstractCommand

logger = logging.getLogger(__name__)


class Takeoff(AbstractCommand):
    def __init__(self, drone: dronekit.Vehicle):
        super().__init__(drone)
        self.id = mavutil.mavlink.MAV_CMD_NAV_TAKEOFF

    def handle(self, altitude: float):
        logger.info(f"Taking off h={altitude}m")
        self._drone.wait_simple_takeoff(alt=altitude, epsilon=0.5)

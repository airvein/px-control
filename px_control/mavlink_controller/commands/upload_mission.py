"""upload_mission.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit

from px_control.common.constants import MissionTypes
from px_control.mavlink_controller.commands.abstract_command import \
    AbstractCommand
from px_control.mavlink_controller.mission.emergency_mission import \
    EmergencyMission
from px_control.mavlink_controller.mission.normal_mission import NormalMission
from px_control.mavlink_controller.mission.rth_mission import RthMission
from px_control.telemetry import Telemetry

logger = logging.getLogger(__name__)


class UploadMission(AbstractCommand):
    def __init__(self, drone: dronekit.Vehicle, telemetry: Telemetry):
        super().__init__(drone)
        self._telemetry = telemetry

        self._mission_type_map = {
            MissionTypes.NORMAL: NormalMission,
            MissionTypes.RTH: RthMission,
            MissionTypes.EMERGENCY: EmergencyMission
        }

    def handle(self, mission_type: MissionTypes, route: str):
        logger.info("Uploading mission...")
        self._telemetry.current_mission = self._mission_type_map[mission_type](
            drone=self._drone, route=route
        )
        self._telemetry.current_mission.prepare()
        self._telemetry.current_mission.upload()

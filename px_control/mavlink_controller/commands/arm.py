"""arm.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from pymavlink import mavutil

from px_control.mavlink_controller.commands.abstract_command import \
    AbstractCommand

logger = logging.getLogger(__name__)


class Arm(AbstractCommand):
    def __init__(self, drone: dronekit.Vehicle):
        super().__init__(drone)
        self.id = mavutil.mavlink.MAV_CMD_COMPONENT_ARM_DISARM

    def handle(self):
        try:
            logger.info("Arming drone...")
            self._drone.arm(timeout=2.0)

        except dronekit.TimeoutError:
            logger.error("Failed to arm drone")

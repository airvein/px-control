"""ack_processor.py


Copyright 2020 Cervi Robotics
"""
import logging
from enum import Enum
from typing import Optional

from pymavlink.mavutil import mavlink

from px_control import settings
from px_control.common.pending_command import PendingCommand
from px_control.telemetry import Telemetry

logger = logging.getLogger(__name__)


class AckStatus(Enum):
    PROCESSING = 'processing'
    SUCCESS = 'success'
    FAILED = 'failed'


class AckProcessor:
    def __init__(self, telemetry: Telemetry, pending_command: PendingCommand):
        self._telemetry = telemetry
        self.pending_command = pending_command

        self.status: AckStatus = AckStatus.PROCESSING
        self.result: Optional[int] = None

        self.repeat_counter: int = settings.AckCounters.REPEAT_COMMAND
        self.check_counter: int = settings.AckCounters.CHECK_ACK

        self._ack_map = {
            None: self._check_ack,
            mavlink.MAV_RESULT_ACCEPTED: self._ack_accepted,
            mavlink.MAV_RESULT_TEMPORARILY_REJECTED: self._ack_temporarily_rejected,
            mavlink.MAV_RESULT_DENIED: self._ack_denied,
            mavlink.MAV_RESULT_FAILED: self._ack_failed,
            mavlink.MAV_RESULT_UNSUPPORTED: self._ack_unsupported,
        }

    def process(self):
        if self.status == AckStatus.PROCESSING:
            self._ack_map[self.result]()

    def _repeat_command(self):
        self.pending_command.command.handle(
            *self.pending_command.args, **self.pending_command.kwargs
        )

    def _check_ack(self):
        if self.check_counter > 0:
            self._get_result()
            self.check_counter -= 1
        else:
            self.status = AckStatus.FAILED
            logger.warning(
                f'NOT RECEIVED ACK FOR COMMAND {self.pending_command.command.id}'
            )

    def _get_result(self):
        updated_commands_ack = []
        for ack in self._telemetry.commands_ack:
            if ack.command == self.pending_command.command.id:
                self.result = ack.result
            else:
                updated_commands_ack.append(ack)

        self._telemetry.commands_ack = updated_commands_ack

    def _ack_accepted(self):
        self.status = AckStatus.SUCCESS
        logger.debug(
            f'COMMAND {self.pending_command.command.id} ACCEPTED'
        )

    def _ack_temporarily_rejected(self):
        if self.repeat_counter > 0:
            self.result = None
            logger.warning(
                f'COMMAND {self.pending_command.command.id} TEMPORARILY REJECTED'
            )
            self._repeat_command()
            self.repeat_counter -= 1
        else:
            self.status = AckStatus.FAILED

    def _ack_denied(self):
        self.status = AckStatus.FAILED
        logger.warning(
            f'COMMAND {self.pending_command.command.id} DENIED'
        )

    def _ack_failed(self):
        self.status = AckStatus.FAILED
        logger.warning(
            f'COMMAND {self.pending_command.command.id} FAILED'
        )

    def _ack_unsupported(self):
        self.status = AckStatus.FAILED
        logger.warning(
            f'COMMAND {self.pending_command.command.id} UNSUPPORTED'
        )

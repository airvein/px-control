"""local_position.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.duplicate_filter import DuplicateFilter
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.telemetry import Telemetry

logger = logging.getLogger(__name__)
logger.addFilter(DuplicateFilter())


class LocalPosition(AbstractMessage):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def handle(self, drone: dronekit.Vehicle, name: str,
               msg: pm.MAVLink_local_position_ned_message):
        logger.debug("Got LOCAL_POSITION_NED")

        if abs(msg.x) < 1e-6 or abs(msg.y) < 1e-6 or abs(msg.z) < 1e-6:
            logger.warning(f"LOCAL POS IS 0!\n"
                           f"X: {msg.x}  Y: {msg.y}  Z: {msg.z}")
            return

        self._telemetry.local_pos.x = msg.x
        self._telemetry.local_pos.y = msg.y
        self._telemetry.local_pos.z = msg.z

        self._telemetry.velocity.vx = msg.vx
        self._telemetry.velocity.vy = msg.vy
        self._telemetry.velocity.vz = msg.vz

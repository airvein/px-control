"""battery_status.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control import settings
from px_control.common.duplicate_filter import DuplicateFilter
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.telemetry import Telemetry

logger = logging.getLogger(__name__)
logger.addFilter(DuplicateFilter())


class BatteryStatus(AbstractMessage):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def handle(self, drone: dronekit.Vehicle, name: str,
               msg: pm.MAVLink_battery_status_message):
        logger.debug("Got BATTERY_STATUS")

        if settings.Connection.AUTOPILOT == "APM":
            return

        #  v == 65535 when autopilot does not have info about voltage
        self._telemetry.battery.voltage = sum(
            [v / 1000.0 for v in msg.voltages if v != 65535]
        )
        self._telemetry.battery.current = msg.current_battery / 100.0
        self._telemetry.battery.current_consumed = msg.current_consumed / 1000.0
        self._telemetry.battery.percent = int(msg.battery_remaining)

"""command_ack.py


Copyright 2020 Cervi Robotics
"""
import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.telemetry import Telemetry, Ack


class CommandAck(AbstractMessage):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def handle(self, drone: dronekit.Vehicle, name: str,
               msg: pm.MAVLink_command_ack_message):
        ack_message = Ack(msg.command, msg.result)
        self._telemetry.commands_ack.append(ack_message)

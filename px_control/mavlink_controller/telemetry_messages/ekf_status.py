"""ekf_status.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.duplicate_filter import DuplicateFilter
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.telemetry import Telemetry

logger = logging.getLogger(__name__)
logger.addFilter(DuplicateFilter())


class EkfStatus(AbstractMessage):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def handle(self, drone: dronekit.Vehicle, name: str,
               msg: pm.MAVLink_ekf_status_report_message):
        logger.debug("Got EKF_STATUS_REPORT")

        self._telemetry.ekf_status.flags = msg.flags
        self._telemetry.ekf_status.velocity_variance = msg.velocity_variance
        self._telemetry.ekf_status.pos_horiz_variance = msg.pos_horiz_variance
        self._telemetry.ekf_status.pos_vert_variance = msg.pos_vert_variance
        self._telemetry.ekf_status.compass_variance = msg.compass_variance

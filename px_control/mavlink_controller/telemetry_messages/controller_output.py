"""controller_output.py


Copyright 2020 Cervi Robotics
"""
import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.telemetry import Telemetry


class ControllerOutput(AbstractMessage):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def handle(self, drone: dronekit.Vehicle, name: str,
               msg: pm.MAVLink_nav_controller_output_message):
        self._telemetry.controller_output = msg

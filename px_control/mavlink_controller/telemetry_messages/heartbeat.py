"""heartbeat.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.duplicate_filter import DuplicateFilter
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.telemetry import Telemetry
from drone_types.msg import PxControlStatus

logger = logging.getLogger(__name__)
logger.addFilter(DuplicateFilter())


class Heartbeat(AbstractMessage):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def handle(self, drone: dronekit.Vehicle, name: str,
               msg: pm.MAVLink_heartbeat_message):
        logger.debug("Got HEARTBEAT")

        self._process_system_status(msg, drone)
        self._process_drone_arm_state(drone)

    def _process_system_status(self, msg: pm.MAVLink_heartbeat_message,
                               drone: dronekit.Vehicle):
        current_mission = self._telemetry.current_mission

        if msg.system_status == pm.MAV_STATE_STANDBY:
            self._telemetry.status.flight_state = \
                PxControlStatus.FLIGHT_STATE_ON_GROUND

        elif current_mission is not None and self._is_in_takeoff(drone):
            self._telemetry.status.flight_state = \
                PxControlStatus.FLIGHT_STATE_TAKEOFF

        elif current_mission is not None and self._is_in_land(drone):
            self._telemetry.status.flight_state = \
                PxControlStatus.FLIGHT_STATE_LANDING

        elif msg.system_status == pm.MAV_STATE_ACTIVE:
            self._telemetry.status.flight_state = \
                PxControlStatus.FLIGHT_STATE_IN_AIR

    def _process_drone_arm_state(self, drone: dronekit.Vehicle):
        if drone.armed:
            self._telemetry.status.arm_state = PxControlStatus.ARM_STATE_ARMED
        elif drone.is_armable:
            self._telemetry.status.arm_state = PxControlStatus.ARM_STATE_READY
        else:
            self._telemetry.status.arm_state = \
                PxControlStatus.ARM_STATE_NOT_READY

    def _is_in_takeoff(self, drone: dronekit.Vehicle):
        current_mission = self._telemetry.current_mission
        return (current_mission.takeoff_index is not None
                and drone.commands.next == current_mission.takeoff_index)

    def _is_in_land(self, drone: dronekit.Vehicle):
        current_mission = self._telemetry.current_mission
        return (current_mission.landing_index is not None
                and drone.commands.next == current_mission.landing_index)

"""attitude_quaternion.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.duplicate_filter import DuplicateFilter
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.telemetry import Telemetry

logger = logging.getLogger(__name__)
logger.addFilter(DuplicateFilter())


class AttitudeQuaternion(AbstractMessage):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def handle(self, drone: dronekit.Vehicle, name: str,
               msg: pm.MAVLink_attitude_quaternion_message):
        logger.debug("Got ATTITUDE_QUATERNION")

        self._telemetry.attitude.q_w = msg.q1
        self._telemetry.attitude.q_x = msg.q2
        self._telemetry.attitude.q_y = msg.q3
        self._telemetry.attitude.q_z = msg.q4
        self._telemetry.attitude.roll_speed = msg.rollspeed
        self._telemetry.attitude.pitch_speed = msg.pitchspeed
        self._telemetry.attitude.yaw_speed = msg.yawspeed

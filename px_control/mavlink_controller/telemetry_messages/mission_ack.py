"""mission_ack.py


Copyright 2020 Cervi Robotics
"""
import os

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.telemetry import Telemetry, MissionAck as Ack


class MissionAck(AbstractMessage):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def handle(self, drone: dronekit.Vehicle, name: str,
               msg: pm.MAVLink_mission_ack_message):
        if self._telemetry.current_mission is not None:
            #  SITL returns error in MISSION_ACK even if the mission is correct
            sitl = os.environ.get('PX_SITL') == '1'
            msg_type = pm.MAV_MISSION_ACCEPTED if sitl else msg.type
            mission_ack = Ack(pm.MAV_MISSION_TYPE_MISSION, msg_type)

            self._telemetry.current_mission.mission_ack = mission_ack

"""imu.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.duplicate_filter import DuplicateFilter
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.telemetry import Telemetry
from drone_types import msg as messages

logger = logging.getLogger(__name__)
logger.addFilter(DuplicateFilter())


class Imu(AbstractMessage):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def handle(self, drone: dronekit.Vehicle, name: str,
               msg: pm.MAVLink_highres_imu_message):
        logger.debug("Got HIGHRES_IMU")

        self._telemetry.imu.acc = messages.Vector3f(x=msg.xacc / 1000.0,
                                                    y=msg.yacc / 1000.0,
                                                    z=msg.zacc / 1000.0)

        self._telemetry.imu.gyro = messages.Vector3f(x=msg.xgyro / 1000.0,
                                                     y=msg.ygyro / 1000.0,
                                                     z=msg.zgyro / 1000.0)

        self._telemetry.imu.mag = messages.Vector3f(x=msg.xmag / 1000.0,
                                                    y=msg.ymag / 1000.0,
                                                    z=msg.zmag / 1000.0)

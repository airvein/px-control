"""attitude_euler.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm
from transforms3d.euler import euler2quat

from px_control import settings
from px_control.common.duplicate_filter import DuplicateFilter
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.telemetry import Telemetry

logger = logging.getLogger(__name__)
logger.addFilter(DuplicateFilter())


class AttitudeEuler(AbstractMessage):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def handle(self, drone: dronekit.Vehicle, name: str,
               msg: pm.MAVLink_attitude_message):
        logger.debug("Got ATTITUDE")

        self._telemetry.attitude.roll_speed = msg.rollspeed
        self._telemetry.attitude.pitch_speed = msg.pitchspeed
        self._telemetry.attitude.yaw_speed = msg.yawspeed

        if settings.Connection.AUTOPILOT == "APM":
            logger.debug("Got ATTITUDE_QUATERNION")

            quaternion = euler2quat(
                msg.rollspeed, msg.pitchspeed, msg.yawspeed, 'rxyz'
            )
            self._telemetry.attitude.q_w = quaternion[0]
            self._telemetry.attitude.q_x = quaternion[1]
            self._telemetry.attitude.q_y = quaternion[2]
            self._telemetry.attitude.q_z = quaternion[3]

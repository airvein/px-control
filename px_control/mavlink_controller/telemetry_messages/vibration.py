"""vibration.py


Copyright 2020 Cervi Robotics
"""
import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control import settings
from px_control.command_interpreter.publishers_manager import PublishersManager
from px_control.common.constants import AlertCodeNames
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.telemetry import Telemetry


class Vibration(AbstractMessage):
    def __init__(self, telemetry: Telemetry,
                 publishers_manager: PublishersManager):
        super().__init__(telemetry)
        self._publishers_manager = publishers_manager

    def handle(self, drone: dronekit.Vehicle, name: str,
               msg: pm.MAVLink_vibration_message):
        self._validate_variance(msg)
        self._telemetry.vibration = msg

    def _validate_variance(self, msg: pm.MAVLink_vibration_message):
        high_vibration = any(
            vibration > settings.Safety.VIBRATION_TOLERANCE
            for vibration in (msg.vibration_x, msg.vibration_y, msg.vibration_z)
        )
        if high_vibration:
            self._publishers_manager.send_alert.publish(
                AlertCodeNames.HIGH_VIBE.name
            )

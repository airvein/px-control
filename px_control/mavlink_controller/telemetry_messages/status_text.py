"""status_text.py


Copyright 2020 Cervi Robotics
"""
import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.command_interpreter.publishers_manager import PublishersManager
from px_control.common.status_text_parser import StatusTextParser
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.settings import Safety
from px_control.telemetry import Telemetry


class StatusText(AbstractMessage):
    def __init__(self, telemetry: Telemetry,
                 publishers_manager: PublishersManager):
        super().__init__(telemetry)
        self._publishers_manager = publishers_manager
        self._parser = StatusTextParser()

    def handle(self, drone: dronekit.Vehicle, name: str,
               msg: pm.MAVLink_statustext_message):

        if msg.severity in Safety.STATUS_TEXT_SEVERITY:
            alert_name = self._parser.match(msg.text)
            if alert_name:
                self._publishers_manager.send_alert.publish(alert_name)

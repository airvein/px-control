"""status.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control import settings
from px_control.common.duplicate_filter import DuplicateFilter
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.telemetry import Telemetry

logger = logging.getLogger(__name__)
logger.addFilter(DuplicateFilter())


class Status(AbstractMessage):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def handle(self, drone: dronekit.Vehicle, name: str,
               msg: pm.MAVLink_sys_status_message):
        logger.debug("Got SYS_STATUS")

        self._telemetry.status.flight_mode = drone.mode.name

        if settings.Connection.AUTOPILOT == "APM":
            self._telemetry.battery.voltage = msg.voltage_battery / 1000.0
            self._telemetry.battery.current = msg.current_battery / 100.0
            self._telemetry.battery.remaining = float(msg.battery_remaining)

"""abstract_message.py


Copyright 2020 Cervi Robotics
"""
from abc import ABC

from px_control.telemetry import Telemetry


class AbstractMessage(ABC):
    def __init__(self, telemetry: Telemetry):
        self._telemetry = telemetry

    def handle(self, *args, **kwargs):
        raise NotImplementedError("Not implemented")

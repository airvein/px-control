"""global_position.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.duplicate_filter import DuplicateFilter
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.telemetry import Telemetry

logger = logging.getLogger(__name__)
logger.addFilter(DuplicateFilter())


class GlobalPosition(AbstractMessage):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def handle(self, drone: dronekit.Vehicle, name: str,
               msg: pm.MAVLink_global_position_int_message):
        logger.debug("Got GLOBAL_POSITION_INT")

        self._telemetry.global_pos.latitude = msg.lat / 1e7
        self._telemetry.global_pos.longitude = msg.lon / 1e7
        self._telemetry.global_pos.altitude = msg.alt / 1000.0

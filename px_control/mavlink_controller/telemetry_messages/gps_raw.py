"""gps_raw.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.duplicate_filter import DuplicateFilter
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.telemetry import Telemetry

logger = logging.getLogger(__name__)
logger.addFilter(DuplicateFilter())


class GpsRaw(AbstractMessage):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def handle(self, drone: dronekit.Vehicle, name: str,
               msg: pm.MAVLink_gps_raw_int_message):
        logger.debug("Got GPS_RAW_INT")

        self._telemetry.global_pos.epv = msg.epv
        self._telemetry.global_pos.eph = msg.eph
        self._telemetry.global_pos.num_sats = msg.satellites_visible

"""scaled_imu.py


Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from pymavlink.dialects.v20 import ardupilotmega as pm

from px_control.common.duplicate_filter import DuplicateFilter
from px_control.mavlink_controller.telemetry_messages.abstract_message import \
    AbstractMessage
from px_control.telemetry import Telemetry
from drone_types import msg as messages

logger = logging.getLogger(__name__)
logger.addFilter(DuplicateFilter())


class ScaledImu(AbstractMessage):
    def __init__(self, telemetry: Telemetry):
        super().__init__(telemetry)

    def handle(self, drone: dronekit.Vehicle, name: str,
               msg: pm.MAVLink_scaled_imu2_message):
        logger.debug("Got SCALED_IMU2")

        # msg.acc in mG; 1G = 9.80665 m/s/s
        self._telemetry.imu.acc = messages.Vector3f(x=msg.xacc * 0.00980665,
                                                    y=msg.yacc * 0.00980665,
                                                    z=msg.zacc * 0.00980665)

        # msg in mrad/s
        self._telemetry.imu.gyro = messages.Vector3f(x=msg.xgyro * 0.001,
                                                     y=msg.ygyro * 0.001,
                                                     z=msg.zgyro * 0.001)

        # msg in mT
        self._telemetry.imu.mag = messages.Vector3f(x=msg.xmag * 0.001,
                                                    y=msg.ymag * 0.001,
                                                    z=msg.zmag * 0.001)

"""commands.py


Copyright 2020 Cervi Robotics
"""
import logging
from queue import Queue
from typing import Optional

import dronekit

from px_control.command_interpreter.publishers_manager import PublishersManager
from px_control.mavlink_controller.ack_processor import AckProcessor, AckStatus
from px_control.mavlink_controller.commands import *
from px_control.common.constants import CommandNames, AlertCodeNames
from px_control.common.pending_command import PendingCommand
from px_control.mavlink_controller.mission_ack_processor import \
    MissionAckProcessor
from px_control.telemetry import Telemetry

logger = logging.getLogger(__name__)


class CommandsManager:
    def __init__(self, drone: dronekit.Vehicle, telemetry: Telemetry,
                 publishers_manager: PublishersManager):
        self._drone = drone
        self._telemetry = telemetry
        self._publishers_manager = publishers_manager
        self._queue: Queue = Queue()

        self._ack_processor: Optional[AckProcessor, MissionAckProcessor] = None
        self._map = {
            CommandNames.ARM: Arm(self._drone),
            CommandNames.SET_GROUNDSPEED: SetGroundspeed(self._drone),
            CommandNames.SET_MODE: SetMode(self._drone),
            CommandNames.TAKEOFF: Takeoff(self._drone),
            CommandNames.TERMINATE_FLIGHT: TerminateFlight(self._drone),
            CommandNames.START_MISSION: StartMission(self._drone),
            CommandNames.UPLOAD_MISSION:
                UploadMission(self._drone, self._telemetry),
        }
        self._command_to_alert_name_map = {
            StartMission: AlertCodeNames.START_MISSION_FAILED,
            UploadMission: AlertCodeNames.UPLOAD_MISSION_FAILED,
            Arm: AlertCodeNames.UNABLE_TO_ARM_DRONE,
            SetMode: AlertCodeNames.UNABLE_TO_CHANGE_MODE
        }

    def add(self, command_name: str, *args, **kwargs):
        pending_command = PendingCommand(
            self._map[command_name], *args, **kwargs
        )
        self._queue.put_nowait(pending_command)

    def process(self):
        if self._ack_processor is None:
            self._process_command()
        else:
            self._process_ack()

    def _process_command(self):
        if not self._queue.empty():
            pending_command = self._queue.get_nowait()

            pending_command.command.handle(
                *pending_command.args, **pending_command.kwargs
            )
            self._create_ack_processor(pending_command)

    def _create_ack_processor(self, pending_command: PendingCommand):
        if isinstance(pending_command.command, UploadMission):
            self._ack_processor = MissionAckProcessor(
                self._telemetry, pending_command)
        else:
            self._ack_processor = AckProcessor(self._telemetry, pending_command)

    def _process_ack(self):
        self._ack_processor.process()
        if self._ack_processor.status == AckStatus.SUCCESS:
            self._ack_processor = None

        elif self._ack_processor.status == AckStatus.FAILED:
            self._send_alert(self._ack_processor.pending_command)
            self._clear_queue()
            self._ack_processor = None

    def _clear_queue(self):
        logger.debug(f'CLEARING QUEUE')
        self._queue = Queue()

    def _send_alert(self, pending_command: PendingCommand):
        alert_name = self._command_to_alert_name_map.get(
            pending_command.command.__class__)
        if alert_name:
            self._publishers_manager.send_alert.publish(alert_name.value)

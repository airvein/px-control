"""mission_ack_processor.py


Copyright 2020 Cervi Robotics
"""
import logging
from typing import Optional

from drone_types.msg import PxControlStatus
from pymavlink.mavutil import mavlink

from px_control.mavlink_controller.mission.emergency_mission import EmergencyMission
from px_control.mavlink_controller.mission.normal_mission import NormalMission
from px_control.mavlink_controller.mission.rth_mission import RthMission
from px_control.settings import AckCounters
from px_control.common.pending_command import PendingCommand
from px_control.telemetry import Telemetry
from px_control.mavlink_controller.ack_processor import AckStatus

logger = logging.getLogger(__name__)


class MissionAckProcessor:
    def __init__(self, telemetry: Telemetry, pending_command: PendingCommand):
        self._telemetry = telemetry
        self.pending_command = pending_command

        self.status: AckStatus = AckStatus.PROCESSING
        self.result: Optional[int] = None

        self.repeat_counter: int = AckCounters.REPEAT_MISSION
        self.check_counter: int = AckCounters.CHECK_MISSION

        self._ack_map = {
            None: self._check_ack,
            mavlink.MAV_MISSION_ACCEPTED: self._ack_accepted,
            mavlink.MAV_MISSION_OPERATION_CANCELLED: self._ack_rejected,
            mavlink.MAV_MISSION_INVALID_SEQUENCE: self._ack_rejected,
            mavlink.MAV_MISSION_DENIED: self._ack_rejected,
        }

        self._mission_state_map = {
            NormalMission: PxControlStatus.MISSION_STATE_NORMAL,
            RthMission: PxControlStatus.MISSION_STATE_RTH,
            EmergencyMission: PxControlStatus.MISSION_STATE_EMERGENCY
        }

    def process(self):
        processing = self.status == AckStatus.PROCESSING
        uploading_mission = self._telemetry.current_mission.uploading
        if processing and not uploading_mission:
            self._ack_map.get(self.result, self._ack_failed)()

    def _repeat_mission(self):
        self.pending_command.command.handle(
            *self.pending_command.args, **self.pending_command.kwargs
        )

    def _check_ack(self):
        if self.check_counter > 0:
            self._get_result()
            self.check_counter -= 1
        else:
            self.status = AckStatus.FAILED
            logger.warning('NOT RECEIVED MISSION ACK')

    def _get_result(self):
        mission = self._telemetry.current_mission
        self.result = mission.mission_ack.result if mission is not None \
            else mavlink.MAV_MISSION_ERROR

    def _ack_accepted(self):
        mission_state = self._mission_state_map[
            self._telemetry.current_mission.__class__
        ]
        self._telemetry.status.mission_state = mission_state
        self.status = AckStatus.SUCCESS
        logger.debug('MISSION ACCEPTED')

    def _ack_rejected(self):
        if self.repeat_counter > 0:
            self.result = None
            logger.warning('MISSION REJECTED')
            self._repeat_mission()
            self.repeat_counter -= 1
        else:
            self._ack_failed()

    def _ack_failed(self):
        self.status = AckStatus.FAILED
        logger.warning('MISSION UPLOAD FAILED')

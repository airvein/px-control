"""rth_mission.py


Copyright 2020 Cervi Robotics
"""

import dronekit

from px_control.mavlink_controller.mission.abstract_mission import \
    AbstractMission
from px_control.mavlink_controller.mission.commands import *
from px_control import settings


class RthMission(AbstractMission):
    def __init__(self, drone: dronekit.Vehicle, route: str):
        super(RthMission, self).__init__(drone, route)

        self._parse_points()
        self._parse_zones()
        self._parse_landing_yaw()

    def prepare(self):
        super(RthMission, self).prepare()

        self.mission_commands.append(
            change_ground_speed.ChangeGroundSpeed(
                ground_speed=settings.Mission.CRUISE_SPEED)
        )
        for point in self._points:
            self.mission_commands.append(
                waypoint.Waypoint(latitude=point[1],
                                  longitude=point[2],
                                  altitude=point[3])
            )
            self._adjust_home_zone_speed(point[0])

        self.mission_commands.append(
            delay.Delay(time=settings.Mission.CHANGE_YAW_DELAY)
        )
        self.mission_commands.append(
            change_yaw.ChangeYaw(self._landing_yaw)
        )
        self.mission_commands.append(
            delay.Delay(time=settings.Mission.LANDING_DELAY)
        )
        self.mission_commands.append(
            land.Land(latitude=self._points[-1][1],
                      longitude=self._points[-1][2])
        )
        self._find_landing_index()

    def _adjust_home_zone_speed(self, point_id: int) -> None:
        if point_id == self._zones['home_maneuver']:
            self.mission_commands.append(
                change_ground_speed.ChangeGroundSpeed(
                    ground_speed=settings.Mission.MANEUVER_SPEED)
            )
        elif point_id == self._zones['home_approach']:
            self.mission_commands.append(
                change_ground_speed.ChangeGroundSpeed(
                    ground_speed=settings.Mission.APPROACH_SPEED)
            )
        elif point_id == self._zones['home_approach'] + 1:
            self.mission_commands.append(
                change_ground_speed.ChangeGroundSpeed(
                    ground_speed=settings.Mission.BEFORE_APPROACH_SPEED[1])
            )
        elif point_id == self._zones['home_approach'] + 2:
            self.mission_commands.append(
                change_ground_speed.ChangeGroundSpeed(
                    ground_speed=settings.Mission.BEFORE_APPROACH_SPEED[0])
            )

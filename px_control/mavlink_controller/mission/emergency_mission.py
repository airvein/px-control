"""emergency_mission.py


Copyright 2020 Cervi Robotics
"""

import dronekit

from px_control.mavlink_controller.mission.abstract_mission import \
    AbstractMission
from px_control.mavlink_controller.mission.commands import *
from px_control import settings


class EmergencyMission(AbstractMission):
    def __init__(self, drone: dronekit.Vehicle, route: str):
        super(EmergencyMission, self).__init__(drone, route)

        self._parse_points()
        self._parse_zones()

    def prepare(self):
        super(EmergencyMission, self).prepare()

        self.mission_commands.append(
            change_ground_speed.ChangeGroundSpeed(
                ground_speed=settings.Mission.CRUISE_SPEED)
        )
        for point in self._points:
            self.mission_commands.append(
                waypoint.Waypoint(latitude=point[1],
                                  longitude=point[2],
                                  altitude=point[3])
            )

        self.mission_commands.append(
            land.Land(latitude=self._points[-1][1],
                      longitude=self._points[-1][2])
        )

        self._find_landing_index()

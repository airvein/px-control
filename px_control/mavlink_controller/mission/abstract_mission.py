"""abstract_mission.py


Copyright 2020 Cervi Robotics
"""
import json
import logging
import threading
from abc import ABC
from typing import Optional, Union, List

import dronekit

from px_control.mavlink_controller.mission.commands import *

logger = logging.getLogger(__name__)

MissionCommandsType = List[
    Union[change_ground_speed.ChangeGroundSpeed, change_yaw.ChangeYaw,
          land.Land, takeoff.Takeoff, waypoint.Waypoint]
]


class MissionAck:
    def __init__(self, mission_type: int, result: int):
        self.mission_type = mission_type
        self.result = result


class AbstractMission(ABC):
    def __init__(self, drone: dronekit.Vehicle, route: str):
        self._drone = drone
        self._route = json.loads(route)
        self.mission_commands: MissionCommandsType = []
        self.mission_ack: Optional[MissionAck] = None

        self._points: Optional[list] = None
        self._zones: Optional[dict] = None
        self._landing_yaw: Optional[float] = None

        self.takeoff_index: Optional[int] = None
        self.landing_index: Optional[int] = None

        self._uploading = False

    @property
    def uploading(self):
        return self._uploading

    def prepare(self):
        logger.debug(f"Preparing: {self.__class__.__name__}")
        self._clear_drone_mission()

        self._drone.commands.download()
        self._drone.commands.wait_ready()

    def upload(self):
        self._uploading = True
        threading.Thread(target=self._upload_mission, daemon=True).start()

    def _upload_mission(self):
        for command in self.mission_commands:
            self._drone.commands.add(command)
        self._drone.commands.upload()
        self._drone.commands.next = 0
        logger.debug(f'Finished uploading: {self.__class__.__name__}')
        self._uploading = False

    def _clear_drone_mission(self):
        self._drone.commands.clear()
        self._drone.commands.upload()

    def _parse_points(self):
        self._points = self._route['route_points']

    def _parse_zones(self):
        self._zones = self._route['zones']

    def _parse_landing_yaw(self):
        self._landing_yaw = self._route['landing_yaw']

    def _find_takeoff_index(self):
        for command in self.mission_commands:
            if isinstance(command, takeoff.Takeoff):
                self.takeoff_index = self.mission_commands.index(command) + 1
                break

    def _find_landing_index(self):
        for command in reversed(self.mission_commands):
            if isinstance(command, land.Land):
                self.landing_index = self.mission_commands.index(command) + 1
                break

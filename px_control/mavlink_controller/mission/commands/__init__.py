__all__ = ['waypoint', 'takeoff', 'land', 'change_yaw', 'change_ground_speed',
           'delay']

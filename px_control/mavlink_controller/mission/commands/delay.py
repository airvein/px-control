"""delay.py


Copyright 2020 Cervi Robotics
"""
from dronekit import Command
from pymavlink import mavutil


class Delay(Command):
    def __init__(self, time: int):
        super(Delay, self).__init__(
            0, 0, 0,
            mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
            mavutil.mavlink.MAV_CMD_NAV_DELAY,
            0, 0, time, -1, -1, -1, 0, 0, 0
        )

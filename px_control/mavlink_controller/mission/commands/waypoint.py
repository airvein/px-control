"""waypoint.py


Copyright 2020 Cervi Robotics
"""
from dronekit import Command
from pymavlink import mavutil


class Waypoint(Command):
    def __init__(self, latitude: float, longitude: float, altitude: float):
        super(Waypoint, self).__init__(
            0, 0, 0,
            mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
            mavutil.mavlink.MAV_CMD_NAV_WAYPOINT,
            0, 0, 0, 0, 0, 0,
            latitude, longitude, altitude
        )

"""land.py


Copyright 2020 Cervi Robotics
"""
from dronekit import Command
from pymavlink import mavutil


class Land(Command):
    def __init__(self, latitude: float, longitude: float):
        super(Land, self).__init__(
            0, 0, 0,
            mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
            mavutil.mavlink.MAV_CMD_NAV_LAND,
            0, 0, 0, mavutil.mavlink.PRECISION_LAND_MODE_REQUIRED,
            0, 0, latitude, longitude, 0
        )

"""change_yaw.py


Copyright 2020 Cervi Robotics
"""
from dronekit import Command
from pymavlink import mavutil


class ChangeYaw(Command):
    def __init__(self, degree: float):
        super(ChangeYaw, self).__init__(
            0, 0, 0,
            mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
            mavutil.mavlink.MAV_CMD_CONDITION_YAW,
            0, 0, degree, 0, 0, 0, 0, 0, 0
        )

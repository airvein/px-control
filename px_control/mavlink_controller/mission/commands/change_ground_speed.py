"""change_ground_speed.py


Copyright 2020 Cervi Robotics
"""
from dronekit import Command
from pymavlink import mavutil


class ChangeGroundSpeed(Command):
    def __init__(self, ground_speed: float):
        super(ChangeGroundSpeed, self).__init__(
            0, 0, 0,
            mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
            mavutil.mavlink.MAV_CMD_DO_CHANGE_SPEED,
            0, 0, 1, ground_speed, 0, 0, 0, 0, 0
        )

import logging

import dronekit

from px_control.command_interpreter.publishers_manager import PublishersManager
from px_control.telemetry import Telemetry
from px_control.common.constants import TelemetryMessagesNames
from px_control.mavlink_controller.telemetry_messages import *
from px_control import settings

logger = logging.getLogger(__name__)


class TelemetryMessagesManager:
    def __init__(self, telemetry: Telemetry, drone: dronekit.Vehicle,
                 publishers_manager: PublishersManager):
        self._telemetry = telemetry
        self._drone = drone
        self._publishers_manager = publishers_manager

        self.map = {
            TelemetryMessagesNames.HEARTBEAT:
                heartbeat.Heartbeat(self._telemetry),
            TelemetryMessagesNames.SYS_STATUS:
                status.Status(self._telemetry),
            TelemetryMessagesNames.BATTERY_STATUS:
                battery_status.BatteryStatus(self._telemetry),
            TelemetryMessagesNames.ATTITUDE:
                attitude_euler.AttitudeEuler(self._telemetry),
            TelemetryMessagesNames.GLOBAL_POSITION_INT:
                global_position.GlobalPosition(self._telemetry),
            TelemetryMessagesNames.GPS_RAW_INT:
                gps_raw.GpsRaw(self._telemetry),
            TelemetryMessagesNames.LOCAL_POSITION_NED:
                local_position.LocalPosition(self._telemetry),
            TelemetryMessagesNames.COMMAND_ACK:
                command_ack.CommandAck(self._telemetry),
            TelemetryMessagesNames.OTHER:
                other.Other(self._telemetry),
            TelemetryMessagesNames.NAV_CONTROLLER_OUTPUT:
                controller_output.ControllerOutput(self._telemetry),
            TelemetryMessagesNames.STATUSTEXT:
                status_text.StatusText(
                    self._telemetry, self._publishers_manager
                ),
            TelemetryMessagesNames.MISSION_ACK:
                mission_ack.MissionAck(self._telemetry),
            TelemetryMessagesNames.ESTIMATOR_STATUS:
                estimator_status.EstimatorStatus(self._telemetry),
            TelemetryMessagesNames.VIBRATION:
                vibration.Vibration(self._telemetry, self._publishers_manager),
        }

        self.autopilot = settings.Connection.AUTOPILOT
        if self.autopilot == "PX4":
            self.map[TelemetryMessagesNames.ATTITUDE_QUATERNION] = \
                attitude_quaternion.AttitudeQuaternion(
                    self._telemetry
                )
            self.map[TelemetryMessagesNames.HIGHRES_IMU] = \
                imu.Imu(self._telemetry)

        elif self.autopilot == "APM":
            self.map[TelemetryMessagesNames.SCALED_IMU] = \
                scaled_imu.ScaledImu(self._telemetry)
            self.map[TelemetryMessagesNames.EKF_STATUS_REPORT] = \
                ekf_status.EkfStatus(self._telemetry)
            self.map[TelemetryMessagesNames.AHRS2] = \
                ahrs2.Ahrs2(self._telemetry)

        for name, message in self.map.items():
            self._drone.add_message_listener(name, message.handle)

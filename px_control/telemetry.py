from typing import Optional, Union, List

from px_control.mavlink_controller.mission.normal_mission import NormalMission
from px_control.mavlink_controller.mission.rth_mission import RthMission
from px_control.mavlink_controller.mission.emergency_mission import \
    EmergencyMission
from pymavlink.dialects.v20.ardupilotmega import (
    MAVLink_ahrs2_message as Ahrs2Msg,
    MAVLink_nav_controller_output_message as ControllerOutputMsg,
    MAVLink_vibration_message as VibrationMsg
)
from drone_types import msg

MissionType = Optional[Union[NormalMission, RthMission, EmergencyMission]]


class Ack:
    def __init__(self, command: int, result: int):
        self.command = command
        self.result = result


class MissionAck:
    def __init__(self, mission_type: int, result: int):
        self.mission_type = mission_type
        self.result = result


class Telemetry:
    def __init__(self):
        # region Publishers
        self.status = msg.PxControlStatus()
        self.ekf_status = msg.PxControlEKFStatus()
        self.battery = msg.PxControlBattery()
        self.attitude = msg.PxControlAttitude()
        self.imu = msg.PxControlIMU()
        self.global_pos = msg.PxControlGlobalPos()
        self.local_pos = msg.PxControlLocalPos()
        self.velocity = msg.PxControlVelocity()
        # endregion

        # region MAVLink
        self.ahrs2 = Ahrs2Msg(
            roll=0.0, pitch=0.0, yaw=0.0, altitude=0.0, lat=0, lng=0
        )
        self.controller_output = ControllerOutputMsg(
            nav_roll=0.0, nav_pitch=0.0, nav_bearing=0, target_bearing=0,
            wp_dist=0, alt_error=0.0, aspd_error=0.0, xtrack_error=0.0
        )
        self.vibration = VibrationMsg(
            time_usec=0, vibration_x=0.0, vibration_y=0.0, vibration_z=0.0,
            clipping_0=0, clipping_1=0, clipping_2=0)
        # endregion

        self.heartbeat_timestamp: int = 0
        self.current_mission: MissionType = None
        self.commands_ack: List[Ack] = []

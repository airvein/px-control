#!/usr/bin/env python3
import logging
from typing import Optional

import dronekit
from rclpy.node import Node

from px_control.command_interpreter.subcribers_manager import SubscribersManager
from px_control.command_interpreter.publishers_manager import PublishersManager
from px_control.common.camera_checker import CameraChecker
from px_control.common.timers import Timers
from px_control.mavlink_controller.commands_manager import CommandsManager
from px_control.mavlink_controller.telemetry_messages_manager import \
    TelemetryMessagesManager
from px_control.telemetry import Telemetry
from px_control.settings import Connection
from drone_types.msg import PxControlStatus

logger = logging.getLogger(__name__)


class PxControl:
    def __init__(self, node: Node):

        self.node = node
        self.telemetry = Telemetry()

        self.drone: Optional[dronekit.Vehicle] = None
        self._connect_drone()

        self.publishers_manager = PublishersManager(
            node=self.node,
            telemetry=self.telemetry
        )
        self.telemetry_messages = TelemetryMessagesManager(
            telemetry=self.telemetry,
            drone=self.drone,
            publishers_manager=self.publishers_manager
        )
        self.commands_manager = CommandsManager(
            drone=self.drone,
            telemetry=self.telemetry,
            publishers_manager=self.publishers_manager
        )
        self.subscribers_manager = SubscribersManager(
            node=self.node,
            telemetry=self.telemetry,
            commands_manager=self.commands_manager
        )
        self.camera_checker = CameraChecker(
            publishers_manager=self.publishers_manager
        )
        self.timers = Timers(node=self.node,
                             commands_manager=self.commands_manager,
                             publishers_manager=self.publishers_manager,
                             camera_checker=self.camera_checker)

    def _connect_drone(self):
        self.telemetry.status.connection_state = \
            PxControlStatus.CONNECTION_STATE_CONNECTING

        if Connection.TYPE == "SERIAL":
            logger.debug(f"Connecting ({Connection.DEVICE})...")

            self.drone = dronekit.connect(Connection.DEVICE, wait_ready=True,
                                          baud=Connection.BAUD)
        elif Connection.TYPE == "UDP":
            logger.debug(f"Connecting ({Connection.URL})...")
            self.drone = dronekit.connect(Connection.URL, wait_ready=True)

        if self.drone is not None:
            self.telemetry.status.connection_state = \
                PxControlStatus.CONNECTION_STATE_CONNECTED

            logger.debug("Connected")
        else:
            logger.fatal("Failed to connect with drone!")

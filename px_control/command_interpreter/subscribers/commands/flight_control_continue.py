"""flight_control_continue.py

Subscriber that handles message with `continue` command

Copyright 2020 Cervi Robotics
"""
import logging

from rclpy.node import Node
from std_msgs import msg

from .abstract_command_subscriber import AbstractCommandSubscriber
from px_control.common.constants import AutopilotModes, CommandNames
from px_control.mavlink_controller.commands_manager import CommandsManager

logger = logging.getLogger(__name__)


class FlightControlContinue(AbstractCommandSubscriber):
    def __init__(self, node: Node, commands_manager: CommandsManager):
        super().__init__(node, commands_manager)

        self.subscription = self._node.create_subscription(
            msg.Empty, "/flight_control/continue", self.handle
        )

    def handle(self, message: msg):
        logger.debug('Received CONTINUE command')
        self._commands_manager.add(CommandNames.SET_MODE, AutopilotModes.AUTO)

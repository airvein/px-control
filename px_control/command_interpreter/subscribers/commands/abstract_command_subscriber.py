"""abstract_command_subscriber.py

Abstract command subscriber that each command subscriber should inherit from.

Copyright 2020 Cervi Robotics
"""
import logging
from abc import ABC

from rclpy.node import Node
from std_msgs import msg

from px_control.mavlink_controller.commands_manager import CommandsManager

logger = logging.getLogger(__name__)


class AbstractCommandSubscriber(ABC):
    def __init__(self, node: Node, commands_manager: CommandsManager):
        self._node = node
        self._commands_manager = commands_manager

    def handle(self, message: msg):
        raise NotImplementedError("Not implemented")

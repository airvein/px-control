"""flight_control_start_mission.py

Subscriber that handles message with `start_mission` command

Copyright 2020 Cervi Robotics
"""
import logging

from rclpy.node import Node
from std_msgs import msg as std_msg

from .abstract_command_subscriber import AbstractCommandSubscriber
from px_control.common.constants import CommandNames, AutopilotModes
from px_control.telemetry import Telemetry
from px_control.mavlink_controller.commands_manager import CommandsManager
from px_control.mavlink_controller.mission.normal_mission import NormalMission
from drone_types import msg

logger = logging.getLogger(__name__)


class FlightControlStartMission(AbstractCommandSubscriber):
    def __init__(self, node: Node, commands_manager: CommandsManager,
                 telemetry: Telemetry):
        super().__init__(node, commands_manager)

        self._telemetry = telemetry

        self.subscription = self._node.create_subscription(
            std_msg.Empty, "/flight_control/start_mission", self.handle
        )

    def handle(self, message: msg):
        logger.debug('Received START_MISSION command')
        if (self._telemetry.status.arm_state !=
                msg.PxControlStatus.ARM_STATE_READY):
            logger.error("Command: 'start_mission' rejected! "
                         "REASON: Drone is NOT ready to arm!")
            return

        if not isinstance(self._telemetry.current_mission, NormalMission):
            logger.error("Command: 'start_mission' rejected! "
                         "REASON: Unexpected type of mission: "
                         f"{type(self._telemetry.current_mission).__name__}")
            return

        self._commands_manager.add(
            CommandNames.SET_MODE, mode=AutopilotModes.GUIDED
        )
        self._commands_manager.add(CommandNames.ARM)
        self._commands_manager.add(CommandNames.START_MISSION)

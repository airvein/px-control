"""flight_control_wait.py

Subscriber that handles message with `wait` command

Copyright 2020 Cervi Robotics
"""
import logging

from rclpy.node import Node
from std_msgs import msg

from px_control.common.constants import CommandNames, AutopilotModes
from .abstract_command_subscriber import AbstractCommandSubscriber
from px_control.mavlink_controller.commands_manager import CommandsManager

logger = logging.getLogger(__name__)


class FlightControlWait(AbstractCommandSubscriber):
    def __init__(self, node: Node, commands_manager: CommandsManager):
        super().__init__(node, commands_manager)

        self.subscription = self._node.create_subscription(
            msg.Empty, "/flight_control/wait", self.handle
        )

    def handle(self, message: msg):
        logger.debug('Received WAIT command')
        self._commands_manager.add(CommandNames.SET_MODE, AutopilotModes.BRAKE)

"""flight_control_land_now.py

Subscriber that handles message with `land_now` command

Copyright 2020 Cervi Robotics
"""
import logging

from rclpy.node import Node
from std_msgs import msg

from .abstract_command_subscriber import AbstractCommandSubscriber
from px_control.mavlink_controller.commands_manager import CommandsManager
from px_control.common.constants import CommandNames, AutopilotModes

logger = logging.getLogger(__name__)


class FlightControlLandNow(AbstractCommandSubscriber):
    def __init__(self, node: Node, commands_manager: CommandsManager):
        super().__init__(node, commands_manager)

        self.subscription = self._node.create_subscription(
            msg.Empty, "/flight_control/land_now", self.handle
        )

    def handle(self, message: msg):
        logger.debug('Received LAND_NOW command')
        self._commands_manager.add(CommandNames.SET_MODE, AutopilotModes.LAND)

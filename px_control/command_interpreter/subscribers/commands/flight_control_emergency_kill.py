"""flight_control_emergency_kill.py

Subscriber that handles message with `emergency_kill` command

Copyright 2020 Cervi Robotics
"""
import logging

from rclpy.node import Node
from std_msgs import msg

from .abstract_command_subscriber import AbstractCommandSubscriber
from px_control.mavlink_controller.commands_manager import CommandsManager
from px_control.common.constants import CommandNames

logger = logging.getLogger(__name__)


class FlightControlEmergencyKill(AbstractCommandSubscriber):
    def __init__(self, node: Node, commands_manager: CommandsManager):
        super().__init__(node, commands_manager)

        self.subscription = self._node.create_subscription(
            msg.Empty, "/flight_control/emergency_kill", self.handle
        )

    def handle(self, message: msg):
        logger.debug('Received EMERGENCY_KILL command')
        self._commands_manager.add(CommandNames.TERMINATE_FLIGHT)

# region route subscribers
from px_control.command_interpreter.subscribers.route.flight_control_route_rth import \
    FlightControlRouteRTH
from px_control.command_interpreter.subscribers.route.flight_control_route_normal import \
    FlightControlRouteNormal
from px_control.command_interpreter.subscribers.route.flight_control_route_emergency import \
    FlightControlRouteEmergency
# endregion

# region commands subscribers
from px_control.command_interpreter.subscribers.commands.flight_control_wait import \
    FlightControlWait
from px_control.command_interpreter.subscribers.commands.flight_control_emergency_kill import \
    FlightControlEmergencyKill
from px_control.command_interpreter.subscribers.commands.flight_control_land_now import \
    FlightControlLandNow
from px_control.command_interpreter.subscribers.commands.flight_control_continue import \
    FlightControlContinue
from px_control.command_interpreter.subscribers.commands.flight_control_start_mission import \
    FlightControlStartMission
# endregion

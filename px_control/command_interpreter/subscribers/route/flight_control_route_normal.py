"""flight_control_route_normal.py

Subscriber that handles message with normal route.

Copyright 2020 Cervi Robotics
"""
import logging

from rclpy.node import Node
from std_msgs import msg

from px_control.common.constants import MissionTypes, CommandNames
from px_control.mavlink_controller.commands_manager import CommandsManager

logger = logging.getLogger(__name__)


class FlightControlRouteNormal:
    def __init__(self, node: Node, commands_manager: CommandsManager):
        self._node = node
        self._commands_manager = commands_manager

        self.subscription = self._node.create_subscription(
            msg.String, "/flight_control/route_normal", self.handle
        )

    def handle(self, message: msg.String):
        self._commands_manager.add(
            CommandNames.UPLOAD_MISSION, MissionTypes.NORMAL, message.data
        )

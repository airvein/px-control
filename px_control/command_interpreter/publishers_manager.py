"""publishers_manager.py

Manager that register ROS publishers to module's node.

Copyright 2020 Cervi Robotics
"""
import logging

from rclpy.node import Node

from px_control.command_interpreter.publishers import *
from px_control.common.constants import PublisherNames
from px_control.telemetry import Telemetry

logger = logging.getLogger(__name__)


class PublishersManager:
    def __init__(self, node: Node, telemetry: Telemetry):
        self._node = node
        self._telemetry = telemetry
        logger.debug("Creating Publishers")

        self.map = {
            PublisherNames.ATTITUDE: Attitude(self._node, self._telemetry),
            PublisherNames.BATTERY: Battery(self._node, self._telemetry),
            PublisherNames.EKF_STATUS: EKFStatus(self._node, self._telemetry),
            PublisherNames.GLOBAL_POS: GlobalPos(self._node, self._telemetry),
            PublisherNames.IMU: IMU(self._node, self._telemetry),
            PublisherNames.LOCAL_POS: LocalPos(self._node, self._telemetry),
            PublisherNames.STATUS: Status(self._node, self._telemetry),
            PublisherNames.VELOCITY: Velocity(self._node, self._telemetry),
        }
        self.heartbeat = Heartbeat(self._node)
        self.send_alert = SendAlert(self._node, self._telemetry)

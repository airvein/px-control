"""global_pos.py


Copyright 2020 Cervi Robotics
"""
from rclpy.node import Node

from .abstract_publisher import AbstractPublisher
from px_control.common.constants import PublisherNames
from px_control.common.utils import get_timestamp
from px_control.telemetry import Telemetry
from drone_types.msg import PxControlGlobalPos


class GlobalPos(AbstractPublisher):
    def __init__(self, node: Node,  telemetry: Telemetry):
        super().__init__(node, telemetry)

        self._publisher = node.create_publisher(
            PxControlGlobalPos, PublisherNames.GLOBAL_POS)

    def publish(self):
        self._telemetry.global_pos.timestamp = get_timestamp()
        self._publisher.publish(self._telemetry.global_pos)

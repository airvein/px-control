"""heartbeat.py


Copyright 2020 Cervi Robotics
"""
from rclpy.node import Node

from px_control.common.constants import PublisherNames
from std_msgs import msg

from px_control.common.utils import get_timestamp


class Heartbeat:
    def __init__(self, node: Node):
        self._publisher = node.create_publisher(
            msg.UInt64, PublisherNames.HEARTBEAT)

    def publish(self):
        message = msg.UInt64(data=get_timestamp())
        self._publisher.publish(message)

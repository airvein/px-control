"""ekf_status.py


Copyright 2020 Cervi Robotics
"""
from rclpy.node import Node

from .abstract_publisher import AbstractPublisher
from px_control.common.constants import PublisherNames
from px_control.common.utils import get_timestamp
from px_control.telemetry import Telemetry
from drone_types.msg import PxControlEKFStatus


class EKFStatus(AbstractPublisher):
    def __init__(self, node: Node,  telemetry: Telemetry):
        super().__init__(node, telemetry)

        self._publisher = node.create_publisher(
            PxControlEKFStatus, PublisherNames.EKF_STATUS)

    def publish(self):
        self._telemetry.ekf_status.timestamp = get_timestamp()
        self._publisher.publish(self._telemetry.ekf_status)

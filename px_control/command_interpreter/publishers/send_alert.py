"""send_alert.py


Copyright 2020 Cervi Robotics
"""
from enum import IntEnum

from rclpy.node import Node

from px_control.common.alert_codes import PreFlightAlertCodes, \
    EmergencyAlertCodes, RthAlertCodes, NormalAlertCodes, AlertCodes
from px_control.common.constants import PublisherNames
from px_control.common.utils import get_timestamp
from px_control.mavlink_controller.mission.emergency_mission import \
    EmergencyMission
from px_control.mavlink_controller.mission.normal_mission import NormalMission
from px_control.mavlink_controller.mission.rth_mission import RthMission
from px_control.telemetry import Telemetry
from drone_types import msg


class SendAlert:
    def __init__(self, node: Node, telemetry: Telemetry):
        self._telemetry = telemetry

        self._alerts_map = {
            NormalMission: NormalAlertCodes,
            RthMission: RthAlertCodes,
            EmergencyMission: EmergencyAlertCodes
        }
        self._publisher = node.create_publisher(
            msg.SendAlert, PublisherNames.SEND_ALERT)

    def publish(self, alert_name: str):
        if alert_name in AlertCodes.__members__.keys():
            alert = AlertCodes[alert_name]
        else:
            alert = self._get_alert_context()[alert_name]

        self._publisher.publish(
            msg.SendAlert(timestamp=get_timestamp(), code=alert.value)
        )

    def _get_alert_context(self) -> IntEnum:
        flight_state = self._telemetry.status.flight_state
        if flight_state == msg.PxControlStatus.FLIGHT_STATE_ON_GROUND:
            return PreFlightAlertCodes
        else:
            return self._alerts_map.get(
                type(self._telemetry.current_mission), PreFlightAlertCodes)

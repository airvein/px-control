"""abstract_publisher.py


Copyright 2020 Cervi Robotics
"""
from abc import ABC

from rclpy.node import Node

from px_control.telemetry import Telemetry


class AbstractPublisher(ABC):
    def __init__(self, node: Node, telemetry: Telemetry):
        self._node = node
        self._telemetry = telemetry

    def publish(self):
        raise NotImplementedError("Not implemented")

"""attitude.py


Copyright 2020 Cervi Robotics
"""
from rclpy.node import Node

from .abstract_publisher import AbstractPublisher
from px_control.common.constants import PublisherNames
from px_control.common.utils import get_timestamp
from px_control.telemetry import Telemetry
from drone_types.msg import PxControlAttitude


class Attitude(AbstractPublisher):
    def __init__(self, node: Node,  telemetry: Telemetry):
        super().__init__(node, telemetry)

        self._publisher = node.create_publisher(
            PxControlAttitude, PublisherNames.ATTITUDE)

    def publish(self):
        self._telemetry.attitude.timestamp = get_timestamp()
        self._publisher.publish(self._telemetry.attitude)

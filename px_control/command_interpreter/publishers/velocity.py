"""velocity.py


Copyright 2020 Cervi Robotics
"""
from rclpy.node import Node

from .abstract_publisher import AbstractPublisher
from px_control.common.constants import PublisherNames
from px_control.common.utils import get_timestamp
from px_control.telemetry import Telemetry
from drone_types.msg import PxControlVelocity


class Velocity(AbstractPublisher):
    def __init__(self, node: Node,  telemetry: Telemetry):
        super().__init__(node, telemetry)

        self._publisher = node.create_publisher(
            PxControlVelocity, PublisherNames.VELOCITY)

    def publish(self):
        self._telemetry.velocity.timestamp = get_timestamp()
        self._publisher.publish(self._telemetry.velocity)

"""subscribers_manager.py

Manager that register ROS subscribers to module's node.

Copyright 2020 Cervi Robotics
"""
import logging

import dronekit
from rclpy.node import Node

from px_control.telemetry import Telemetry
from px_control.mavlink_controller.commands_manager import CommandsManager
from px_control.command_interpreter.subscribers import *

logger = logging.getLogger(__name__)


class SubscribersManager:
    def __init__(self, node: Node, telemetry: Telemetry,
                 commands_manager: CommandsManager):
        logger.debug("Creating Subscribers")

        self._route = {
            "flight_control_route_normal":
                FlightControlRouteNormal(node, commands_manager),
            "flight_control_route_emergency":
                FlightControlRouteEmergency(node, commands_manager),
            "flight_control_route_rth":
                FlightControlRouteRTH(node, commands_manager),
        }
        self._commands = {
            "flight_control_land_now":
                FlightControlLandNow(node, commands_manager),
            "flight_control_emergency_kill":
                FlightControlEmergencyKill(node, commands_manager),
            "flight_control_wait":
                FlightControlWait(node, commands_manager),
            "flight_control_continue":
                FlightControlContinue(node, commands_manager),
            "flight_control_start_mission":
                FlightControlStartMission(node, commands_manager, telemetry),
        }

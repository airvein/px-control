from enum import Enum, IntEnum

from px_control import PACKAGE_NAME


class PublisherNames:
    STATUS = f'/{PACKAGE_NAME}/status'
    EKF_STATUS = f'/{PACKAGE_NAME}/ekf_status'
    BATTERY = f'/{PACKAGE_NAME}/battery'
    ATTITUDE = f'/{PACKAGE_NAME}/attitude'
    IMU = f'/{PACKAGE_NAME}/imu'
    LOCAL_POS = f'/{PACKAGE_NAME}/local_pos'
    GLOBAL_POS = f'/{PACKAGE_NAME}/global_pos'
    VELOCITY = f'/{PACKAGE_NAME}/velocity'
    HEARTBEAT = f'/{PACKAGE_NAME}/heartbeat'
    SEND_ALERT = '/send_alert'


class TelemetryMessagesNames:
    HEARTBEAT = 'HEARTBEAT'
    EXTENDED_SYS_STATE = 'EXTENDED_SYS_STATE'
    SYS_STATUS = 'SYS_STATUS'
    BATTERY_STATUS = 'BATTERY_STATUS'
    RADIO_STATUS = 'RADIO_STATUS'
    ALTITUDE = 'ALTITUDE'
    ATTITUDE = 'ATTITUDE'
    ATTITUDE_QUATERNION = 'ATTITUDE_QUATERNION'
    GLOBAL_POSITION_INT = 'GLOBAL_POSITION_INT'
    GPS_RAW_INT = 'GPS_RAW_INT'
    LOCAL_POSITION_NED = 'LOCAL_POSITION_NED'
    HIGHRES_IMU = 'HIGHRES_IMU'
    SCALED_IMU = 'SCALED_IMU2'
    EKF_STATUS_REPORT = 'EKF_STATUS_REPORT'
    AHRS2 = 'AHRS2'
    COMMAND_ACK = 'COMMAND_ACK'
    OTHER = 'OTHER'
    NAV_CONTROLLER_OUTPUT = 'NAV_CONTROLLER_OUTPUT'
    STATUSTEXT = 'STATUSTEXT'
    ESTIMATOR_STATUS = 'ESTIMATOR_STATUS'
    MISSION_ACK = 'MISSION_ACK'
    VIBRATION = 'VIBRATION'


class MissionTypes(Enum):
    NORMAL = 'NORMAL'
    RTH = 'RTH'
    EMERGENCY = 'EMERGENCY'


class CommandNames:
    ARM = 'arm'
    SET_GROUNDSPEED = 'set_groundspeed'
    SET_MODE = 'set_mode'
    SET_TARGET_POSITION = 'set_target_position'
    SET_TARGET_VELOCITY = 'set_target_velocity'
    SET_YAWRATE_VELOCITY = 'set_yawrate_velocity'
    TAKEOFF = 'takeoff'
    TERMINATE_FLIGHT = 'terminate_flight'
    START_MISSION = 'start_mission'
    UPLOAD_MISSION = 'upload_mission'


class AutopilotModes:
    STABILIZE = "STABILIZE"
    AUTO = "AUTO"
    GUIDED = "GUIDED"
    OFFBOARD = "OFFBOARD"
    RTL = "RTL"
    BRAKE = "BRAKE"
    LAND = "LAND"


class AlertCodeNames(Enum):
    HIGH_VIBE = 'HIGH_VIBE'
    START_MISSION_FAILED = 'START_MISSION_FAILED'
    UPLOAD_MISSION_FAILED = 'UPLOAD_MISSION_FAILED'
    UNABLE_TO_ARM_DRONE = 'UNABLE_TO_ARM_DRONE'
    UNABLE_TO_CHANGE_MODE = 'UNABLE_TO_CHANGE_MODE'
    CAMERA_NOT_CONNECTED = 'CAMERA_NOT_CONNECTED'
    STREAM_OFFLINE = 'STREAM_OFFLINE'


class StatusTextMessages(Enum):
    GPS_GLITCH = 'GPS Glitch'
    ACCELS_INCONSISTENT = 'Accels inconsistent'
    BAD_AHRS = 'Bad AHRS'
    COMPASSES_INCONSISTENT = 'Compasses inconsistent'
    POTENTIAL_THRUST_LOSS = 'Potential Thrust Loss'
    INTERNAL_ERROR = 'internal error'
    BAD_GPS_HEALTH = 'Bad GPS Health'
    BAD_LIDAR_HEALTH = 'Bad LiDAR Health'
    BAD_OPTIFLOW_HEALTH = 'Bad OptFlow Health'
    GYROS_INCONSISTENT = 'Gyros inconsistent'
    BAD_OR_NO_TERRAIN_DATA = 'Bad or No Terrain Data'
    ERROR_COMPASS_VARIANCE = 'Error compass variance'
    BAD_LOGGING = 'Bad Logging'
    GPS_FALLING_CONFIGURATION_CHECKS = 'GPS failing configuration checks'
    EKF_VARIANCE = 'EKF variance'
    BAD_BARO_HEALTH = 'Bad Baro Health'
    GYROS_NOT_HEALTHY = 'Gyros not healthy'
    BAD_VISION_POSITION = 'Bad Vision Position'

from rclpy.node import Node
from rclpy.timer import WallTimer

from px_control import settings
from px_control.command_interpreter.publishers_manager import PublishersManager
from px_control.common.camera_checker import CameraChecker
from px_control.mavlink_controller.commands_manager import CommandsManager


class Timers:
    def __init__(self, node: Node, commands_manager: CommandsManager,
                 publishers_manager: PublishersManager,
                 camera_checker: CameraChecker):
        self.node = node
        self.commands_manager = commands_manager
        self.publishers_manager = publishers_manager
        self.camera_checker = camera_checker

        self._create_timers()

    def _commands_executor_callback(self):
        self.commands_manager.process()

    def _telemetry_callback(self):
        for publisher in self.publishers_manager.map.values():
            publisher.publish()

    def _heartbeat_callback(self):
        self.publishers_manager.heartbeat.publish()

    def _camera_callback(self):
        self.camera_checker.check()

    def _create_timers(self):
        self.telemetry: WallTimer = self.node.create_timer(
            1.0 / settings.Timers.TELEMETRY_UPDATE_FREQ,
            self._telemetry_callback
        )
        self.commands_executor: WallTimer = self.node.create_timer(
            1.0 / settings.Timers.COMMANDS_EXECUTE_FREQ,
            self._commands_executor_callback
        )
        self.heartbeat: WallTimer = self.node.create_timer(
            1.0 / settings.Timers.HEARTBEAT_UPDATE_FREQ,
            self._heartbeat_callback
        )
        if settings.Safety.Camera.AVAILABLE:
            self.camera: WallTimer = self.node.create_timer(
                1.0 / settings.Timers.CAMERA_CHECKER_UPDATE_FREQ,
                self._camera_callback
            )

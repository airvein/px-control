import os

from px_control.command_interpreter.publishers_manager import PublishersManager
from px_control.common.constants import AlertCodeNames
from px_control.settings import Safety


class CameraChecker:
    def __init__(self, publishers_manager: PublishersManager):
        self.publishers_manager = publishers_manager

    def check(self):
        if not self._camera_available:
            self.publishers_manager.send_alert.publish(
                AlertCodeNames.CAMERA_NOT_CONNECTED.value
            )
        if not self._stream_available:
            self.publishers_manager.send_alert.publish(
                AlertCodeNames.STREAM_OFFLINE.value
            )

    @property
    def _camera_available(self):
        return Safety.Camera.CAMERA_NAME in os.listdir('/dev')

    @property
    def _stream_available(self):
        feed_status = os.system(
            f'systemctl is-active --quiet {Safety.Camera.FEED_SERVICE}'
        )
        stream_status = os.system(
            f'systemctl is-active --quiet {Safety.Camera.STREAM_SERVICE}'
        )
        return feed_status == 0 and stream_status == 0

import logging


class DuplicateFilter(logging.Filter):
    def __init__(self):
        super().__init__()
        self.logs = set()

    def filter(self, record):
        current_log = (record.module, record.levelno, record.msg)
        if current_log not in self.logs:
            self.logs.add(current_log)
            return True
        else:
            return False

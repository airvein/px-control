from px_control.mavlink_controller.commands import *
from typing import Union

CommandType = Union[Takeoff, SetGroundspeed, SetMode,
                    StartMission, Arm, TerminateFlight]


class PendingCommand:
    def __init__(self, command, *args, **kwargs):
        self.command: CommandType = command
        self.args = args
        self.kwargs = kwargs

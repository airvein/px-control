import re
from typing import Dict, Optional

from px_control.common.constants import StatusTextMessages


class StatusTextParser:
    def __init__(self):
        self._patterns_map: Dict[str, StatusTextMessages] = {
            self._prepare_pattern(item): item for item in StatusTextMessages
        }

    def match(self, text: str) -> Optional[str]:
        for pattern, status_text_message in self._patterns_map.items():
            if re.match(pattern, text.upper()):
                return status_text_message.name

    @staticmethod
    def _prepare_pattern(text: StatusTextMessages) -> str:
        """Returns pattern that contains '.*' between every word in text
        """
        pattern_list = [r'^.*']
        for w in text.value.split():
            pattern_list.append(w.upper())
            pattern_list.append(r'.*')

        pattern_list.append(r'$')
        pattern = r''.join(pattern_list)
        return pattern

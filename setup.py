from setuptools import setup

package_name = 'px_control'

setup(
    name=package_name,
    version='0.6.0',
    py_modules=[
        'px_run'
    ],
    packages=[
        'px_control',
    ],
    data_files=[
        ('share/' + package_name, [
            'package.xml',
        ]),
    ],
    install_requires=[
        'setuptools'
    ],
    zip_safe=True,
    author='Marcin Kornat',
    author_email='marcin.kornat@cervirobotics.com',
    maintainer='Marcin Kornat',
    maintainer_email='marcin.kornat@cervirobotics.com',
    classifiers=[
        'Topic :: Software Development :: Embedded Systems'
    ],
    keywords=['ROS'],
    description=['Pixhawk flight controller communication module'],
    license='Copyright 2018 Cervi Robotics sp. z o.o, all rights reserved',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'px_run = px_run:main'
        ]
    }
)

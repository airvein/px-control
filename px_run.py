#!/usr/bin/env python3
from datetime import datetime
import logging
import os

import rclpy

from px_control.px_control import PxControl
from px_control import PACKAGE_NAME

logger = logging.getLogger(PACKAGE_NAME)


def initialize_logger():
    log_dir = os.environ.get("AIRVEIN_LOGS_PATH") or \
              os.path.join(os.path.expanduser("~"), "airvein_ws", "flight_logs")
    date = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    log_filename = f'{PACKAGE_NAME}-{date}.log'
    log_path = os.path.join(log_dir, log_filename)

    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s [%(levelname)s] %(name)s: %(message)s",
        handlers=[
            logging.FileHandler(log_path),
            logging.StreamHandler()
        ]
    )
    autopilot_logger = logging.getLogger('autopilot')
    autopilot_logger.setLevel(level=logging.ERROR)


def main(args=None):
    initialize_logger()

    rclpy.init(args=args)
    node = rclpy.create_node(PACKAGE_NAME)

    px_control = PxControl(node=node)

    try:
        while rclpy.ok():
            try:
                rclpy.spin_once(node)
            except KeyboardInterrupt:
                break
    finally:
        rclpy.shutdown()


if __name__ == '__main__':
    main()

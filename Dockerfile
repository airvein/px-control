FROM git.cervirobotics.com:5005/airvein/tools/ros2-build:dashing

RUN mkdir -p /workspace/src

ADD requirements.txt /tmp/requirements.txt
ADD apts.txt /tmp/apts.txt

ENV APTS=/tmp/apts.txt

RUN mkdir -p ~/.config/pip/ \
    && python3 -m pip install -r /tmp/requirements.txt \
    && apt-get update \
    && bash -c "xargs -a <(awk '! /^ *(#|$)/' \"$APTS\") -r -- apt-get install -y -q" \
    && rm -rf /var/lib/apt/lists/*


COPY px-control /workspace/src/px-control
COPY drone-types /workspace/src/drone-types


WORKDIR /workspace

RUN /bin/bash -c "source /opt/ros/dashing/setup.bash && colcon build"
